#!/usr/bin/python3

#region ----- CHECK PRESENCE OF MODULES -----

def check_module(name:str, error_message:str, instruction:str):
	import sys
	name			= str(name)
	error_message	= str(error_message)
	instruction		= str(instruction)
	try:
		dummy = __import__(name)
	except Exception as x:
		print('\x1b[1;91m%s\x1b[0m' % error_message)
		print(instruction)
		sys.exit(1)

check_module("fpdf", "De module 'fpdf' is niet gevonden.", "Installeer met: $ pip3 install fpdf")

#endregion

import sys
import webbrowser
from   objects.applicatie 			import Applicatie
import utils.print_functions 		as PF
import utils.filesystem_functions	as FS

# ----- CONSTANTS AND GLOBAL VARIABLES -----

CONFIG_FILE = './start.cfg'

#region ----- USAGE -----

def print_usage(error=None):
	print()
	PF.green('Beschrijving:')
	print   ('    Een script om MyHeritage-data in te lezen, te doorzoeken, te converteren enz.')
	PF.green('Gebruik:')
	print   ('    $ ./start.py                : Print deze beschrijving.')
	print	('    $ ./start.py check          : Checkt of bestanden/directories gevonden kunnen worden.')
	print	('    $ ./start.py alle           : Print alle entiteiten.')
	print	('    $ ./start.py personen       : Print alle persoon-entiteiten.')
	print	('    $ ./start.py families       : Print alle familie-entiteiten.')
	print	('    $ ./start.py bronnen        : Print alle bron-entiteiten.')
	print	('    $ ./start.py overige        : Print alle andere entiteiten (metadata en albums).')
	print	('    $ ./start.py zoek <woord>   : Print personen met een woord in een van de naamvelden.')
	print	('    $ ./start.py id <id>        : Print een entiteit met bepaalde ID. Syntax:')
	print	('                                  @F123@, @f123@, of ook f123 of zelfs 456 (dit alleen voor personen).')
	print	('    $ ./start.py maak teksten   : Maakt (opnieuw) de directory met losse tekstbestandjes.')
	print	('    $ ./start.py maak json      : Maakt (opnieuw) de directory met losse JSON-bestandjes (JavaScript Object Notation).')
	print	('    $ ./start/py maak pdf       : Maakt (opnieuw) de directory met losse PDF-bestandjes.')
	print	('    $ ./start.py maak xml       : Maakt (opnieuw) de directory met losse XML-bestandjes (Extensible Markup Language).')
	print	('    $ ./start.py maak html      : Maakt (opnieuw) de HTML-directory met webpagina\'s.')
	print	('    $ ./start.py html [start]   : Start FireFox met de ingestelde start-pagina (of anders de index-pagina).')
	print	('    $ ./start.py html index     : Start FireFox met de hiervoor gemaakte index-pagina.')
	print	('    $ ./start.py html id   <id> : Start FireFox met de pagina voor genoemd ID.')
	print	('    $ ./start.py formulier <id> : Maakt een tekstbestand voor de familie met het gegeven id.')
	PF.yellow('Tip:')
	print	('    Normaal spuugt het script in 1x alles uit wat je opvraagt; met het commando less kan je dit eventueel pagina-gewijs doen.')
	print	('    Om daarbij te kleuropmaak te handhaven, moet je echter de -R optie gebruiken, bijv.:')
	print	('    $ .start.py alle | less -R           (afbreken met Q)')
	PF.green('Gebruik ontwikkelaar:')
	print	('    $ ./start.py repareer       : Zoekt naar coderingsfouten van MyHeritage (met name è en ë aan het einde van regels).')
	print	('    $ ./start.py dump -1        : Print alle tekstregels die niet begrepen worden.')
	print	('    $ ./start.py dump <n>       : Print alle tekstregels die beginnen met een getal <= n.')
	print	('    $ ./start.py lees [<n>]     : Vertaalt alle tekstregels naar records, en print ze dan (optioneel max n regels).')
	print	('    $ ./start.py regels  <id>   : Idem, maar print alleen de letterlijke regels voor genoemde id.')
	print	('    $ ./start.py regels  <m> <n>: Idem, maar print alleen de regels binnen het gegeven bereik.')
	print	('    $ ./start.py records <m> <n>: Idem, maar print alleen de records binnen het gegeven bereik. (Sommige records beslaan meer regels.)')
	print	('    $ ./start.py filter  <woord>: Idem, maar print alleen de regels die het woord bevatten (niet hoofdl.gev.)')
	print	('    $ ./start.py filterx <woord>: Idem, maar exact (hoofdlettergevoelig).)')
	print	('    $ ./start.py verwerk [<n>]  : Vertaalt alle tekstregels naar records en verwerkt ze tot entiteiten (optioneel max n regels).')
	print 	('    $ ./start.py test           : Alleen voor ad-hoc ontwikkelaar-tests.')
	PF.green('Laatste wijzigingen:')
	print	('    02-12-2021: Extern: Export naar JSON-bestanden.')
	print	('    05-12-2021: Extern: Export naar XML-bestanden.')
	print	('    10-12-2021: Extern: Export naar PDF-bestanden.')
	print	('    27-04-2022: Afbeeldingen toegevoegd aan PDFs.')
	print	('    02-02-2023: Module-check toegevoegd (ivm pfdf), plus check/reparatie corrupte MyHeritage-coderingen; e.a.')
	PF.green('Versie:')
	print   ('    20230202A')
	print()
	if error:
		PF.red('Fout:')
		for line in error.split('\n'):
			PF.red('    ' + line)
		sys.exit(1)
	else:
		sys.exit(0)

#endregion

#region ----- HELPER-FUNCTIONS -----

def print_entiteiten(entiteiten):
	PF.yellow("(Aantal: %i)" % len(entiteiten))
	for entiteit in entiteiten:
		print()
		PF.yellow('_' * 50)
		print(entiteit.naar_tekst())
	PF.yellow("(Aantal: %i)" % len(entiteiten))

#endregion

#region ----- MAIN() -----

args		= list(sys.argv)
argn 		= len(args)

if argn == 1:
	# FINISH WHITHOUT AN ERROR-MESSAGE:
	print_usage()

app = Applicatie(CONFIG_FILE)
app.lees_configuratie()

if argn == 2 and args[1] == 'check':
	app.check_configuratie()

elif argn == 2 and args[1] == 'repareer':
	app.repareer_gedcom()

elif argn == 3 and args[1] == 'dump':
	level = int(args[2])
	app.dump_regels(level)

elif argn >= 2 and args[1] == 'lees':
	if argn == 2:
		# ALLES:
		app.print_records()
	else:
		# MAX. AANTAL:
		app.print_records(int(args[2]))

elif argn == 3 and args[1] == 'regels':
	app.print_regels_id(args[2])

elif argn == 4 and args[1] == 'regels':
	app.print_records(eerste_regel=int(args[2]), laatste_regel=int(args[3]))

elif argn == 4 and args[1] == 'records':
	app.print_records(eerste_record=int(args[2]), laatste_record=int(args[3]))

elif argn == 3 and args[1] == 'filter':
	app.print_records(filter=str(args[2]))

elif argn == 3 and args[1] == 'filterx':
	app.print_records(filter=str(args[2]), exact=True)

elif argn >= 2 and args[1] == 'verwerk':
	if argn == 2:
		# ALLES:
		entiteiten = app.maak_entiteiten()
	else:
		# MAX. AANTAL:
		entiteiten = app.maak_entiteiten(int(args[2]))

elif argn == 2 and args[1] == 'alle':
	entiteiten 	= app.geef_alle()
	print_entiteiten(entiteiten)

elif argn == 2 and args[1] == 'personen':
	entiteiten 	= app.geef_personen()
	print_entiteiten(entiteiten)

elif argn == 2 and args[1] == 'families':
	entiteiten 	= app.geef_families()
	print_entiteiten(entiteiten)

elif argn == 2 and args[1] == 'bronnen':
	entiteiten 	= app.geef_bronnen()
	print_entiteiten(entiteiten)

elif argn == 2 and args[1] == 'overige':
	entiteiten 	= app.geef_overige()
	print_entiteiten(entiteiten)

elif argn == 3 and args[1] == 'zoek':
	entiteiten 	= app.zoek_personen(str(args[2]))
	print_entiteiten(entiteiten)

elif argn == 3 and args[1] == 'id':
	entiteiten 	= app.zoek_id(str(args[2]))
	print_entiteiten(entiteiten)

elif argn == 3 and args[1] == 'maak' and args[2] == 'html':
	app.maak_html()

elif argn == 3 and args[1] == 'maak' and args[2] == 'json':
	app.maak_json()

elif argn == 3 and args[1] == 'maak' and args[2] == 'pdf':
	app.maak_pdf()

elif argn == 3 and args[1] == 'maak' and args[2] == 'teksten':
	app.maak_tekstbestanden()

elif argn == 3 and args[1] == 'maak' and args[2] == 'xml':
	app.maak_xml()

elif (argn == 2 and args[1] == 'html') or (argn == 3 and args[1] == 'html' and args[2] == 'start'):
	browser = webbrowser.get('firefox')
	if app.html_start:
		browser.open_new_tab(app.html_start)
	else:
		browser.open_new_tab(app.html_index)

elif argn == 3 and args[1] == 'html' and args[2] == 'index':
	browser = webbrowser.get('firefox')
	browser.open_new_tab(app.html_index)

elif argn == 4 and args[1] == 'html' and args[2] == 'id':
	url = app.html_id(args[3])
	if url:
		browser = webbrowser.get('firefox')
		browser.open_new_tab(url)
	else:
		PF.red('Bestand bestaat niet!')

elif argn == 3 and args[1] == 'formulier':
	pad = app.maak_formulier(str(args[2]))
	if pad:
		print("Gelukt! Inhoud van het bestand %s:" % pad)
		print()
		tekst = FS.read_unicode(pad)
		print(tekst)
	else:
		print('Mislukt! Was het ID wel van een familie?')

elif argn == 2 and args[1] == 'test':
	app.test()	# 'PRIVATE' TEST-FUNCTION

else:
	print_usage('De argumenten zijn niet correct.\nLees de beschrijving en probeer opnieuw!')

#endregion