#!/usr/bin/bash
# Script waarmee de source-directory "my-heritage" bijgewerkt wordt met alle wijzigingen op de GitLab-server.

cd my-heritage

echo "____________________________________________________"
echo "Stap 1) Haal alle wijzingingen van de GitLab-server:"
echo "(Enter om door te gaan, CTRL+C om af te breken)"
read input
git fetch --all

echo "____________________________________________________"
echo "Stap 2) Opgehaalde wijzingingen:"
echo
git log HEAD..origin/master

echo "____________________________________________________"
echo "Stap 3) Integreer de wijzigingen hier:"
echo "(Enter om door te gaan, CTRL+C om af te breken)"
read input
git merge

echo "____________________________________________________"
echo "Klaar! Recente historie:"
echo
git log -40 --date=format:"%Y-%m-%d %H:%M" --pretty=format:"%h %ad %s" --reverse --all
echo

