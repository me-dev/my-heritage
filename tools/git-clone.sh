#!/usr/bin/bash
# Script om eenmalig de source op te halen van GitLab.
# Er wordt hiermee een subdirectory "my-heritage" gemaakt in de huidige directory.
# Deze kan integraal (met zijn verborgen subdirectories) gekopieerd naar om het even welke plaat.

git clone https://gitlab.com/me-dev/my-heritage.git
