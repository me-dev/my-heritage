import os

# ----- APPLICATION-SPECIFIC FUNCTIONS -----

# ----- COPIED STANDARD FUNCTIONS -----

def copy_file(path_from, path_to, overwrite=True):
	'''
	Description: Copies a file, optionally overwriting an already existing target.
	Parameters : path_from: Must exist and may not be a directory.
	-            path_to  : May not be a directory, so its up to the caller to supply a full path.
	-            overwrite: By default True. If False, an error will be raised when existing.
	Returnvalue: True if an existing target had to be deleted.
	Changes    : 31-08-2020 Created
	Version    : 20200831A
	'''
	import os
	import shutil
	path_from	= str(path_from)
	path_to		= str(path_to)
	overwrite	= bool(overwrite)

	result		= False

	# CHECKS:
	if os.path.isdir(path_from):
		raise IsADirectoryError('Source is a directory, not a file: %s' % path_from)
	if not os.path.isfile(path_from):
		raise FileNotFoundError('Source does not exist, so it cant be copied: %s' % path_from)
	if os.path.isdir(path_to):
		raise IsADirectoryError('Target is a directory, not a file: %s' % path_to)
	if os.path.isfile(path_to):
		if overwrite:
			os.remove(path_to)
			result = True
		else:
			raise FileExistsError('File %s exists and will not be overwritten.' % path_to)

	# COPY:
	shutil.copy2(path_from, path_to, follow_symlinks=True)
	return result

def create_directory(path):
	'''
	Description: Creates (the complete path to) a directory, if it does not exist yet.
	Returnvalue: True if it had to be created, and False if it already existed.
	Changes    : 24-04-2020 Created
	Version    : 20200424A
	'''
	import os
	path = str(path)
	if os.path.isdir(path):
		return False
	else:
		os.makedirs(path, exist_ok=True)
		return True

def datetime_modified(path):
	'''
	Description: Returns the modification-datetime of a file/directory, and None if not existing.
	Changes    : 24-04-2020 Created
	Version    : 20200424A
	'''
	import datetime
	import os
	path = str(path)
	if not os.path.exists(path):
		return None
	else:
		stat = os.stat(path)
		return datetime.datetime.fromtimestamp(stat.st_mtime)

def delete_directory(path):
	'''
	Description: Deletes a directory and all files and subdirectories within it!
	Returnvalue: True if it had to be deleted, and False if it already didnt exist.
	Changes    : 24-04-2020 Created
	Version    : 20200424A
	'''
	import os
	import shutil
	path = str(path)
	if os.path.isdir(path):
		shutil.rmtree(path)
		return True
	else:
		return False

def delete_file(path):
	'''
	Description: Deletes the file if it exists.
	Returnvalue: True if it had to be deleted, and False if it already didnt exist.
	Changes    : 25-04-2020 Created
	Version    : 20200425A
	'''
	import os
	path = str(path)
	if os.path.isfile(path):
		os.remove(path)
		return True
	else:
		return False

def exists_directory(path):
	'''
	Description: Returns True if the path exists and is a directory.
	Changes    : 24-04-2020 Created
	Version    : 20200424A
	'''
	import os
	path = str(path)
	return os.path.isdir(path)

def exists_file(path):
	'''
	Description: Returns True if the path exists and is a file.
	Changes    : 25-04-2020 Created
	Version    : 20200425A
	'''
	import os
	path = str(path)
	return os.path.isfile(path)

def extension_image(filename):
	'''
	Description: Returns True if the extension of the filename is/suggests an image.
	Example    : True, for strings like: /my/path/my.gif my.gif MY.GIF .gif gif
	Remarks    : This function does not check the existence or the actual contents of a file.
	-            It just inspects the string; anything that is found after the last dot or else the whole string.
	Changes    : 26-04-2021 Created
	Version    : 20210426A
	'''
	if not filename:
		return False
	filename = str(filename).lower()
	pos = filename.rfind('.')
	if pos >= 0:
		extension = filename[pos+1:]
	else:
		extension = filename
	return extension in ['bmp', 'dib', 'gif', 'ico', 'jpe', 'jpeg', 'jpg', 'png', 'tif', 'tiff']

def extension_image(filename):
	'''
	Description: Returns True if the extension of the filename is/suggests an image.
	Example    : True, for strings like: /my/path/my.gif my.gif MY.GIF .gif gif
	Remarks    : This function does not check the existence or the actual contents of a file.
	-            It just inspects the string; anything that is found after the last dot or else the whole string.
	Changes    : 26-04-2021 Created
	Version    : 20210426A
	'''
	if not filename:
		return False
	filename = str(filename).lower()
	pos = filename.rfind('.')
	if pos >= 0:
		extension = filename[pos+1:]
	else:
		extension = filename
	return extension in ['bmp', 'dib', 'gif', 'ico', 'jpe', 'jpeg', 'jpg', 'png', 'tif', 'tiff']

def file_open(path):
	'''
	Description: Opens a non-executable file in the default application of the OS.
	Parameters : path: Must exist
	Example    : file_open('./some.pdf')
	Changes    : 06-12-2021 Created
	Version    : 20211206A
	'''
	import os
	import subprocess
	path = str(path)
	if not os.path.isfile(path):
		raise ValueError("Path for file to open does not exist: '%s'" % path)
	subprocess.call(["xdg-open", path])

def move_file(source_file, target_dir, new_name=None, raise_error=True):
	'''
	Description: Moves a file to another location.
	Parameters : source_file: Must exist.
	-            target_dir : Must exist; the directory, the file will be copied into.
	-            new_name   : By default, the file will remain its original name; anyhow, the target-file must not exist.
	-            raise_error: If False an error will be printed and False returned.
	Returnvalue: True if succeeded; False will only be returned when raise_error=False.
	Changes    : 08-07-2022 Created
	Version    : 20220708A
	'''
	import os
	source_file = str(source_file)
	target_dir	= str(target_dir)
	new_name	= str(new_name) if new_name else None
	raise_error	= bool(raise_error)

	# CHECK SOURCE-FILE:
	if not os.path.isfile(source_file):
		msg = "The source-file does not exist: '%s'" % source_file
		if raise_error:
			raise ValueError(msg)
		else:
			print(msg)
			return False

	# CHECK TARGET-DIR:
	if not os.path.isdir(target_dir):
		msg = "The target-directory does not exist: '%s'" % target_dir
		if raise_error:
			raise ValueError(msg)
		else:
			print(msg)
			return False

	# CHECK TARGET-FILE:
	if not new_name:
		new_name = os.path.basename(source_file)
	target_file = os.path.join(target_dir, new_name)
	if os.path.isfile(target_file):
		msg = "The target-file already exists: '%s'" % target_file
		if raise_error:
			raise ValueError(msg)
		else:
			print(msg)
			return False

	# DO THE WORK:
	try:
		os.rename(source_file, target_file)
		return True
	except Exception as x:
		if raise_error:
			raise x
		else:
			print(str(x))
			return False

def name_sec(path):
	'''
	Description: Gives the name of a file or directory without the parent-path.
	-            Differs from os.path.basename() that it checks both for \ and /.
	-            (In exports by other apps paths can be produced, different from the syntax in Linux.
	Parameters : path: the full path to a file or directory.
	Returnvalue: Literally, everything after the last \ or /, else the path as it is.
	Example    : name_sec('..\export\pics') -> 'pics'
	Changes    : 26-04-2021 Created
	Version    : 20210426A
	'''
	if path == None:
		return None
	path	= str(path)
	pos1	= path.rfind('/')
	pos2	= path.rfind('\\')
	pos		= pos1 if pos1 >= pos2 else pos2
	if pos == -1:
		return path
	else:
		return path[pos+1:]

def path_absolute(path):
	'''
	Description: Resolves relative parts (. and ..) in a path.
	-            Uses os.path.abspath() but is more consistent in the results.
	Returnvalue: For None and '', the input will be returned as it is.
	Changes    : 26-04-2021 Created
	Version    : 20210426A
	'''
	import os
	if not path:
		return path
	path = str(path)
	return os.path.abspath(path)

def read_cfg(path=None, keys=None, debug=False):
	'''
	Description: Translates for a .py-file an accompanying .cfg-file into a dictionary.
	-            All valid lines will result in key-values.
	Parameters : path : An optional path to a configuration-file elsewhere; by default <file>.cfg is used for a <file>.py with this function.
	-            keys : An optional but recommended list with the keys the caller expects to be present. Detects obsolete .cfg-files.
	-            debug: For testing, prints the result.
	Errors     : FileNotFoundError, IOError, KeyError.
	Returnvalue: A dictionary (unsorted) with all key-values for valid lines.
	Example    : my_dict = read_cfg(['source', 'target'])
	Remarks    : The .cfg-file should contain comment with the rules to be applied:
 	-            1) Lines not starting with a letter (A-Z, a-z) will be skipped.
	-            2) Lines not containing an '=' will be skipped.
	-            3) The text left  of '=' will be trimmed and used as key.
	-            4) An empty key will be skipped.
	-            5) A key already defined will be overwritten, so the last one wins.
	-            6) The text right of '=' will be taken literally, incl. spaces and tabs, up to the newline but not including it.
	-               Beware that some text-editors remove trailing whitespace automatically!
	-            7) If you need newlines in values, escape and translate them in your script with e.g.: \\n, CRLF etc.
 	Changes    : 05-03-2021 Created
 	-            06-04-2021 Added parameter path, so that this function also can be used residing in a library-file elsewhere.
	Version    : 20210406A
	'''
	import os

	# CHECKS:
	if keys:
		keys = list(keys)
	debug = bool(debug)
	if path:
		cfg_file = path
	else:
		cfg_file = __file__[0:-3] + '.cfg'
	if not os.path.isfile(cfg_file):
		raise FileNotFoundError("The function read_cfg() did not find the file: " + cfg_file)
	if not os.access(cfg_file, os.R_OK):
		raise FileNotFoundError("The function read_cfg() cannot read the file: " + cfg_file)

	# PARSE THE CFG-FILE:
	result = {}
	with open(cfg_file) as cfg:
		for line in cfg:
			line = line.strip('\n')
			if not line:
				continue
			if not line[0].upper() in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
				continue
			pos = line.find('=')
			if pos == -1:
				continue
			key = line[0:pos].strip()
			if key:
				result[key] = line[pos+1:]

	# PRINT RESULT?
	if debug:
		for key in sorted(result.keys()):
			print('"%s"="%s"' % (key, result[key]))

	# CHECK KEYS?
	if keys:
		cfg_keys = result.keys()
		for key in keys:
			if key not in cfg_keys:
				raise KeyError('The following key was expected but not found: ' + key)

	return result

def read_lines(path, stripnewlines=False):
	'''
	Description: Returns the lines found in a text-file.
	Parameters : stripnewlines: When False, each line will retain its original line-ending; when True all \\n and \\r will be stripped.
	Remarks    : Naively assumes that the encoding of the contents will be understandable for Python.
	Changes    : 09-05-2020 Created
	Version    : 20200509A
	'''
	import os
	path = str(path)
	if not os.path.isfile(path):
		raise FileNotFoundError('File not found: ' + path)
	if stripnewlines:
		lines = [line.strip('\r\n') for line in open(path)]
	else:
		lines = [line for line in open(path)]
	return lines

def read_lines_raw(path, newline=b'\n', strip=True) -> list:
	'''
	Description: Returns the raw byte-lines of a file, so without decoding the bytes yet.
	-            Made for finding corrupt encodings in gedcom-files exported from MyHeritage.
	newline    : So for gedcom we would use: b'\r\n'
	strip      : By-default the newline itself will not be returned in the raw lines.
	Changes    : 02-02-2023 Created
	Version    : 20230202A
	'''
	import os
	path	= str(path)
	strip	= bool(strip)

	if not os.path.isfile(path):
		raise FileNotFoundError('File not found: ' + path)

	result		= []
	start		= 0
	len_newline	= len(newline)

	with open(path, 'rb') as f:
		data:bytes = f.read()
		while True:
			pos = data.find(newline, start)
			if pos < 0:
				result.append(data[start:])
				break
			else:
				if strip:
					result.append(data[start:pos])
				else:
					result.append(data[start:pos+len_newline])
				start = pos + len_newline
	return result

def read_records_utf8(path, separator='\n'):
	'''
	Description: Returns the records found in a UTF8 text-file.
	-            Is more versatile than read_lines_utf8(), because that function both splits on \\n and \\r\\n.
	-            (Files from MyHeritage for example contain \\r\\n to separate records and \\n to separate lines in long texts.)
	Parameters : separator    : Defaults to \\n, but can be overridden.
	Uses       : read_utf8(path)
	Returnvalue: A list with records that never will contain the separator itself.
	Changes    : 26-04-202 Created
	Version    : 20210426A
	'''
	import os
	path		= str(path)
	separator	= str(separator)
	if not os.path.isfile(path):
		raise FileNotFoundError('File not found: ' + path)

	text 	= read_utf8(path)
	records	= list(text.split(separator))
	return records

def read_unicode(path):
	'''
	Description: Returns the (decoded) text from the Unicode-file.
	-          : A BOM at the 1st line will be removed.
	Changes    : 25-04-2020 Created
	-            19-06-2020 Added removal of BOM.
	Version    : 20200619A
	'''
	import os

	path = str(path)
	if not os.path.isfile(path):
		raise FileNotFoundError('File not found: ' + path)

	with open(path, 'rb') as f:
		bytes = f.read()

		# DO WE HAVE TO SKIP THE BOM?
		if len(bytes) >= 2 and bytes[0] == 255 and bytes[1] == 254:
			return bytes[2:].decode('utf_16_le')
		else:
			return bytes.decode('utf_16_le')

def read_utf8(path):
	'''
	Description: Returns the (decoded) text from the Unicode-file.
	-          : A BOM at the 1st line will be removed.
	Changes    : 25-04-2020 Created
	-            19-06-2020 Added removal of BOM.
	Version    : 20200619A
	'''
	import os

	path = str(path)
	if not os.path.isfile(path):
		raise FileNotFoundError('File not found: ' + path)

	with open(path, 'rb') as f:
		bytes = f.read()

		# DO WE HAVE TO SKIP THE BOM?
		if len(bytes) >= 3 and bytes[0] == 239 and bytes[1] == 187 and bytes[2] == 191:
			return bytes[3:].decode('utf_8')
		else:
			return bytes.decode('utf_8')

def size_readable(size, decimals):
	'''
	Description: Generates a human-readable string for a (large) integer number: 1024 -> 1KB etc.
	-            Without decimals, the letters will be attached to the number.
	-            Otherwise, for better readability a space will separate the number and the letters.
    Returnvalue: Used letters are:
	-            b (bytes), KB (kilobytes), MB (mega), GB (giga), TB (tera),
	-            PB (peta), EB (exa), ZB (zetta), YB (yotta), BB (bronto)
	-            and, not knowing the shortcut: ?B (geop)
	Changes    : 24-04-2020 Created
	-            26-04-2021 Improved parameter-check.
	Version    : 20210426A
	'''
	units    = ['b', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB', 'BB', '?B']
	size     = int(size)     if size     else 0
	decimals = int(decimals) if decimals else 0
	if decimals < 0:
		decimals = 0

	# NORMALLY, WE RETURN FROM WITHIN THIS LOOP:
	for unit in units[:-1]:
		if size < 1024:
			if decimals == 0:
				# OMIT A SPACE:
				return '{:.{}f}{}'.format(size, decimals, unit)
			else:
				# INSERT A SPACE FOR BETTER READABILITY:
				return '{:.{}f} {}'.format(size, decimals, unit)
		size /= 1024

	# BUT CATCH EVEN GREATER NUMBERS:
	if decimals == 0:
		# OMIT A SPACE:
		return '{:.{}f}{}'.format(size, decimals, units[-1])
	else:
		# INSERT A SPACE FOR BETTER READABILITY:
		return '{:.{}f} {}'.format(size, decimals, units[-1])

def write_bytes(data:bytes, path:str):
	'''
	Description: Writes bytes to a file.
	-            Overwrites an existing file.
	Changes    : 24-04-2022 Created
	Version    : 20220424A
	'''
	path = str(path)
	with open(path, 'wb') as f:
		f.write(data)

def write_unicode(text, path):
	'''
	Description: Writes normal text to a Unicode little-endian file, with BOM.
	-            Overwrites an existing file.
	-            It is up to the caller to code the end-of-lines within the text: \\r\\n.
	'''
	UNICODE_BOM	= b'\xff\xfe'
	with open(path, 'wb') as f:
		f.write(UNICODE_BOM)
		f.write(text.encode('utf_16_le'))

def write_utf8(text, path):
	'''
	Description: Writes normal text to a UTF-8 file, with BOM.
	-            Overwrites an existing file.
	-            It is up to the caller how end-of-lines are coded within the text: with \\n or \\r\\n.
	Changes    : 25-04-2020 Created
	Version    : 20200425A
	'''
	text	 = str(text)
	path	 = str(path)
	UTF8_BOM = b'\xef\xbb\xbf'

	with open(path, 'wb') as f:
		f.write(UTF8_BOM)
		f.write(text.encode('utf_8'))
