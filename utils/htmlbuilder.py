import html

class HtmlBuilder:
	'''
	Description: A class for simplifying creating HTML-documents, for instance for reports.
	-            It does not check HTML-logic itself, so you are free to add list-items to tables or table-rows to divs etc.
	Changes    : 15-04-2021 Created
	-            26-04-2021 Finalized improvements.
	Version    : 20210426A
	'''

	def __init__(self, attributes=None, fragment=False):
		self.fragment 	= None
		self.doc		= None
		self.head		= None
		self.body		= None
		if fragment:
			self.fragment = Fragment()
		else:
			self.doc 	= Document()
			self.head	= self.doc.head		# A SHORTCUT FOR CONVENIENCE.
			self.body	= self.doc.body		# IDEM

	def __repr__(self):
		if self.fragment:
			return repr(self.fragment)
		else:
			return repr(self.doc)

	def to_html(self, tab='\t', tabs=0):
		if self.fragment:
			return self.fragment.to_html(tab, tabs)
		else:
			return self.doc.to_html(tab, tabs)

	def html_decode(self, text, twice=False):
		'''
		Translate something like "&" or "<" into "&amp;" and "&lt;".
		twice: MyHeritage has some texts encoded 2 times!
		'''
		twice = bool(twice)
		if twice:
			return html.unescape(html.unescape(text))
		else:
			return html.unescape(text)

	def html_encode(self, text):
		'''Translate something like "&amp;" or "&lt;" back into "&" and "<".'''
		return html.escape(text)

class Tag():
	'''The base class for all other tags. It could be used directly for new tags not having their own subclass.'''

	def __init__(self, tagname, attributes=[], inline=True):
		'''
		attributes: a list of tuples with name-value pairs (both strings).
		inline    : whether (for readability) newlines should be added (like for hr an div) or not (like for img and span).
		'''
		self.name			= str(tagname)
		self.attributes		= list(attributes) if attributes else []
		self.inline			= bool(inline)

	def __repr__(self):
		return "Tag (%s, attributes=%s)" % (self.name, self.attributes)

	def to_html(self, tab='\t', tabs=0):
		'''Should never be hit!'''
		indentation = (tab * tabs ) if tab and tabs else ''
		return "%sTag (%s, attributes=%s)%s" % (indentation, self.name, self.attributes, '\n' if not self.inline else '')

class SingleTag(Tag):
	'''The base class for tags like br, hr and img.'''

	def __init__(self, tagname, attributes=[], inline=True):
		'''Attributes must be list of tuples with name-value pairs (both strings).'''
		super().__init__(tagname, attributes, inline)

	def __repr__(self):
		return "SingleTag (%s, attributes=%s)" % (self.name, self.attributes)

	def to_html(self, tab='\t', tabs=0):
		indentation = (tab * tabs ) if tab and tabs else ''
		result 		= "%s<%s" % (indentation, self.name)
		for (name, value) in self.attributes:
			result += " %s='%s'" % (name, value)
		result += "/>"
		if self.inline:
			return result
		else:
			return result + '\n'

class ContainerTag(Tag):
	'''The base class for all tags that can contain other tags.'''

	def __init__(self, tagname, attributes=[], inline=False):
		'''Attributes must be list of tuples with name-value pairs (both strings).'''
		super().__init__(tagname, attributes, inline)
		self.childtags = []

	def __repr__(self):
		return "ContainerTag (%s, attributes=%s)" % (self.name, self.attributes)

	def to_html(self, tab='\t', tabs=0):
		indentation = (tab * tabs ) if tab and tabs else ''
		result 		= "%s<%s" % (indentation, self.name)
		for name, value in self.attributes:
			result += " %s='%s'" % (name, value)
		result += '>'
		if not self.inline:
			result += '\n'
		for childtag in self.childtags:
			result += childtag.to_html(tab, tabs+1)
		if not self.inline and not result.endswith('\n'):
			result += '\n'
		result += "%s</%s>" % (indentation, self.name)
		if not self.inline:
			result += '\n'
		return result

	def _last_childtag(self, name):
		'''Returns the last childtag with that name or None.'''
		for childtag in self.childtags[::-1]:
			if childtag.name == name:
				return childtag
		# NOTHING FOUND, SO:
		return None

	@property
	def div(self):
		'''Returns the last div or None.'''
		return self._last_childtag('div')

	@div.setter
	def div(self, obj):
		'''Adds a new div to the childtags.'''
		if not isinstance(obj, Div):
			raise TypeError('Wrong object-type for .div')
		self.childtags.append(obj)

	@property
	def tr(self):
		'''Returns the last tr or none.'''
		return self._last_childtag('tr')

	@tr.setter
	def tr(self, obj):
		'''Adds a new tr to the childtags.'''
		if not isinstance(obj, Tr):
			raise TypeError('Wrong object-type for .tr')
		self.childtags.append(obj)

	@property
	def th(self):
		'''Returns the last th or none.'''
		return self._last_childtag('th')

	@th.setter
	def th(self, obj):
		'''Adds a new th to the childtags.'''
		if not isinstance(obj, Th):
			raise TypeError('Wrong object-type for .th')
		self.childtags.append(obj)

	@property
	def td(self):
		'''Returns the last td or none.'''
		return self._last_childtag('td')

	@td.setter
	def td(self, obj):
		'''Adds a new td to the childtags.'''
		if not isinstance(obj, Td):
			raise TypeError('Wrong object-type for .td')
		self.childtags.append(obj)

	@property
	def text(self):
		'''Returns the last text or None.'''
		return self._last_childtag('(text)')

	@text.setter
	def text(self, value):
		'''Supports as value a Text-object or literal text.'''
		if isinstance(value, Text):
			self.childtags.append(value)
		else:
			self.childtags.append(Text(str(value)))

	@property
	def literal(self):
		'''Returns the last literal or None.'''
		return self._last_childtag('(literal)')

	@literal.setter
	def literal(self, value):
		'''Supports as value a Literal-object or literal text.'''
		if isinstance(value, Literal):
			self.childtags.append(value)
		else:
			self.childtags.append(Literal(str(value)))

	@property
	def link(self):
		'''Returns the last link (a) or None.'''
		return self._last_childtag('a')

	@link.setter
	def link(self, obj):
		'''Adds a new link (a) to the childtags.'''
		if not isinstance(obj, Link):
			raise TypeError('Wrong object-type for .link')
		self.childtags.append(obj)

	@property
	def img(self):
		'''Returns the last img or None.'''
		return self._last_childtag('img')

	@img.setter
	def img(self, obj):
		'''Adds a new img to the childtags.'''
		if not isinstance(obj, Image):
			raise TypeError('Wrong object-type for .img')
		self.childtags.append(obj)

	def add_tr_headers(self, *headers, attributes=[]):
		'''Shortcut for adding a tr and a number of ths inside it.'''
		self.tr = Tr()
		for header in headers:
			self.tr.th = Th(attributes=attributes, text=header)

	def add_tr_empty(self, count=1):
		'''Shortcut for adding 1 or more empty rows = rows with an empty cell.'''
		for i in range(count):
			self.tr = Tr()
			self.tr.td = Td()
			self.tr.td.literal = Literal('&nbsp;')

	def add_tr_property(self, propname, value, skip_none=True, escape=True):
		'''
		Shortcut for adding a tr with 2 tds, the first with class=label.
		escape: Some sources of data deliver HTML, which can be added literally.
		-       This function can make the decision self if escape is explicitly set to None.
		-       If it finds some HTML-content, it will not escape it.
		'''
		value 		= str(value) if value else ''
		skip_none	= bool(skip_none)
		if escape == None and value != None:
			tests 	= ['<br>', '<br/>', '&amp;']
			escape	= True
			for test in tests:
				if value.find(test) > -1:
					escape = False
					break
		else:
			escape	= bool(escape)

		if (value == None or value == '') and skip_none:
			return

		self.tr = Tr()
		attributes = [('class', 'label')]
		self.tr.td = Td(attributes=attributes, text=propname)
		if escape:
			self.tr.td = Td(text=value)
		else:
			self.tr.td = Td()
			self.tr.td.literal = Literal(value)

	def add_tr_proplink(self, propname, caption, url, target=None):
		'''Shortcut for adding a tr with 2 tds, the first with class=label, the second with a hyperlink.'''
		self.tr = Tr()
		self.tr.td = Td(attributes=[('class', 'label')], text=propname)
		self.tr.td = Td()
		self.tr.td.link = Link(caption, url, target=target)

	def add_tr_propimg(self, propname, url, attributes):
		'''Shortcut for adding a tr with 2 tds, the first with class=label, the second with an image.'''
		self.tr = Tr()
		self.tr.td = Td(attributes=[('class', 'label')], text=propname)
		self.tr.td = Td()
		self.tr.td.img = Image(propname, url, attributes)

	def add_literal(self, value):
		'''Shortcut for adding HTML produced elsewhere.'''
		if isinstance(value, Literal):
			self.childtags.append(value)
		else:
			self.childtags.append(Literal(str(value)))

class Fragment(ContainerTag):
	'''In essence an invisible tag, containing other visible tags.'''

	def __init__(self):
		super().__init__('(fragment)')

	def to_html(self, tab='\t', tabs=0):
		indentation = (tab * tabs ) if tab and tabs else ''
		result = ''
		for childtag in self.childtags:
			result += childtag.to_html(tab, tabs)
		return result

class Document(ContainerTag):
	def __init__(self, attributes=[]):
		super().__init__('html', attributes)
		self.childtags.append(Head())
		self.childtags.append(Body())

	@property
	def head(self):
		return self.childtags[0]

	@property
	def body(self):
		return self.childtags[1]

class Head(ContainerTag):
	def __init__(self):
		super().__init__('head', attributes=None)

class Body(ContainerTag):
	def __init__(self, attributes=[]):
		super().__init__('body', attributes)

class Div(ContainerTag):
	def __init__(self, attributes=[], text=None):
		super().__init__('div', attributes)
		if text:
			self.childtags.append(Text(str(text)))

class Tr(ContainerTag):
	def __init__(self, attributes=[]):
		super().__init__('tr', attributes)

class Th(ContainerTag):
	def __init__(self, attributes=[], text=None):
		super().__init__('th', attributes)
		if text:
			self.childtags.append(Text(str(text)))

class Td(ContainerTag):
	def __init__(self, attributes=[], text=None):
		super().__init__('td', attributes)
		if text:
			self.childtags.append(Text(str(text)))

class Text(Tag):
	'''Is treated as a Tag, but one with an empty name, no attributes and only text (that will be encoded).'''

	def __init__(self, text):
		super().__init__('(text)', attributes=None, inline=True)
		self.text = str(text)

	def to_html(self, tab='\t', tabs=0):
		indentation = (tab * tabs ) if tab and tabs else ''
		return indentation + html.escape(self.text)

class Literal(Tag):
	'''Like Text: Is treated as a Tag, but one with an empty name, no attributes and only literal HTML-text (that should have been encoded).'''

	def __init__(self, text):
		super().__init__('(literal)', attributes=None, inline=True)
		self.text = str(text)

	def to_html(self, tab='\t', tabs=0):
		return self.text

class Link(ContainerTag):
	def __init__(self, caption, url, target=None, attributes=[]):
		'''Attributes must be list of tuples with name-value pairs (both strings).'''
		attributes = list(attributes)	# (MAKES A COPY)
		attributes.append(('href', str(url)))
		if target:
			attributes.append(('target', str(target)))
		super().__init__('a', attributes, inline=True)
		self.childtags.append(Text(str(caption)))

class Image(SingleTag):
	def __init__(self, caption, url, attributes=[]):
		'''Attributes must be list of tuples with name-value pairs (both strings).'''
		attributes = list(attributes)	# (MAKES A COPY)
		attributes.append(('alt', str(caption)))
		attributes.append(('src', str(url)))
		super().__init__('img', attributes, inline=True)
