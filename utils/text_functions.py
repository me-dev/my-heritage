import html

# ----- APPLICATION-SPECIFIC FUNCTIONS -----


# ----- COPIED FUNCTIONS -----

def html_decode(text, twice=False, remove_tags=False, insert_newlines=False):
	'''
	Description: Translate something like "&amp;" or "&lt;" back into "&" and "<".
	Parameters : twice          : MyHeritage has some texts encoded 2 times! (Probably markdown and html.)
	-            remove_tags    : Sometimes the text itself is HTML with tags and attributes, and we are only interested in the text-content.
	-            insert_newlines: When True, block-tags like </div> and </p> will be replaced by a newline, else by a space.
	Example    : html_decode('&#x27; &quot; &amp; &lt; &gt;') --> ' " & < >
	Changes    : 26-04-2021 Created
	-            08-12-2021 Added parameter remove_tags.
	-            09-12-2021 Added parameter insert_newlines.
	Version    : 20211209A
	'''
	import html

	text			= str(text) if text else ''
	twice			= bool(twice)
	remove_tags		= bool(remove_tags)
	insert_newlines	= bool(insert_newlines)

	if remove_tags:
		cleaned		= ''
		inside_tag	= False
		i			= -1
		for letter in text:
			i += 1
			if letter == '<':
				inside_tag = True
			if not inside_tag:
				cleaned += letter
			if letter == '>':
				inside_tag = False
				# FOR SOME TAGS IT IS NICER TO ADD A SPACE OR NEWLINE:
				if text[i-3:i+1] in ['</p>', '</P>', '<br>', '<BR>']:   # (MARKDOWN USES THE INCORRECT <br>)
					cleaned += '\n' if insert_newlines else ' '
				elif text[i-4:i+1] in ['<br/>', '<BR/>']:				# (OFFICIALLY, IS SHOULD BE <br/>)
					cleaned += '\n' if insert_newlines else ' '
				elif text[i-5:i+1] in ['</div>', '</DIV>']:
					cleaned += '\n' if insert_newlines else ' '
		text = cleaned.strip()

	# BETTER REPLACE NON-BREAKABLE SPACES WITH NORMAL SPACES:
	text = text.replace('&nbsp;', ' ')
	text = html.unescape(text)
	if twice:
		text = text.replace('&nbsp;', ' ')
		text = html.unescape(text)
	return text

def html_encode(text):
	'''
	Description: Translate something like "&" or "<" into "&amp;" and "&lt;".
	Example    : html_encode('' " & < >') --> '&#x27; &quot; &amp; &lt; &gt;'
	Changes    : 26-04-2021 Created
	Version    : 20210624A
	'''
	import html
	text = str(text) if text else ''
	return html.escape(text)

def replace_illegals(text, encoding, replace=True):
	'''
	Description: Replaces Unicode-chars that a certain encoding does not accept.
	Parameters : encoding: The regular Python-encodings, like 'iso8859_1'.
	-            replace : When True, puts a '?' instead of the illegal letter; when False, skips the letter.
	Remarks    : Initially created for the builtin fonts in fpdf, that do not support all Unicode chars.
	Example    : replace_illegals('female:♀ male:♂ currencies:$€£¥', 'iso8859_1') --> 'female:? male:? currencies:$?£¥'
	-            replace_illegals('...idem...', 'iso8859_1', replace=False)       --> 'female: male: currencies:$£¥'
	Changes    : 09-12-2021 Created
	Version    : 20211209A
	'''
	replace = bool(replace)
	if text != None:
		text = str(text)
		if replace:
			bytes = text.encode(encoding, errors='replace')
		else:
			bytes = text.encode(encoding, errors='ignore')
		text  = bytes.decode(encoding) # ILLEGAL CHARS PERHAPS ARE REPLACED WITH '?'.
	return text

def timestamp(dtm=None, size=14):
	'''
	Description: Returns a string for the local time like: '20171231235959'.
	Parameters : If no dtm is given, the current datetime will be used.
	-            With size you can specify how many characters you want (def. = 14).
	-            8 gives you the date-part, 14 date+time-parts, larger values even msecs.
	Changes    : 24-04-2020 Created
	Version    : 20200424A
	'''
	import datetime
	if dtm == None:
		dtm = datetime.datetime.now()
	if size == None:
		size = 14
	result = dtm.strftime('%Y%m%d%H%M%S%f')
	return result[0:size]

# ----- COPIED CLASS -----

class StandardStringBuilder:
	'''
	Description: Wrapper for io.StringIO.
	Changes    : 28-04-2020 Created
	-            14-04-2021 Improved for empty values, added parameter for multiple newlines.
	-            15-06-2021 Bugfix method and field both having the name 'text'.
	Version    : 20210615A
	'''

	def __init__(self):
		from io import StringIO
		self._sb 	= StringIO()
		self._text	= None			# ONLY USED WHEN CLOSING, TO KEEP THE RESULT.
		self.closed = False

	def add(self, text):
		if text:
			self._sb.write(str(text))

	def addline(self, text=None, count=1):
		if text:
			self._sb.write(str(text))
		for i in range(int(count)):
			self._sb.write('\n')

	def text(self):
		if self.closed:
			return self._text
		else:
			return self._sb.getvalue()

	def clear(self):
		from io import StringIO
		self._sb = StringIO()

	def close(self):
		'''Returns the accumulated text.'''
		self._text = self._sb.getvalue()
		self._sb.close()
		self.closed = True
		return self._text
