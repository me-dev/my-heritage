class Metadata:
	'''A set of rules telling what to do with text-lines.'''

	def __init__(self, items):
		'''items: a list of tuples that will be read into rules.'''
		items = list(items)
		self.rules = []
		for tuple in items:
			self.rules.append(Rule(tuple))

class Rule:
	'''A single metadata-item with fields: prop, number, type and context.'''

	def __init__(self, tuple):
		self.prop		= str(tuple[0])
		self.number		= int(tuple[1])
		self.type		= str(tuple[2])
		self.context	= str(tuple[3])
		self.caption	= self.prop[0].capitalize() + self.prop[1:].replace('_', ' ')	# (KEEP 1ST UNDERSCORE)