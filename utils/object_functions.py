# ----- COPIED STANDARD-FUNCTIONS -----

def instantiate(classname, *args, **kwargs):
	'''
	Instantiates an object only using a classname and the arguments for __init__().
	(Currently this function requires knowledge of the classes present in the project.)
	'''

	# EXTEND WHEN NECESSARY:
	if classname == 'Adres':
		from objects.adres import Adres
	elif classname == 'Beroep':
		from objects.beroep import Beroep
	elif classname == 'Bestand':
		from objects.bestand import Bestand
	elif classname == 'Bron':
		from objects.bron import Bron
	elif classname == 'Doop':
		from objects.doop import Doop
	elif classname == 'Gebeurtenis':
		from objects.gebeurtenis import Gebeurtenis
	elif classname == 'Huwelijk':
		from objects.huwelijk import Huwelijk
	elif classname == 'Opleiding':
		from objects.opleiding import Opleiding

	# INSTANTIATE ...
	if classname in locals():
		cls	= locals()[classname]
		obj	= cls.__new__(cls)
		obj.__init__(*args, **kwargs)
		return obj
	else:
		# ... OR EXPLAIN WHAT TO DO:
		raise NotImplementedError("The class '%s' is not found by object_functions.instantiate(). Add an import-statement to this function!" % classname)
