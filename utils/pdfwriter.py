import fpdf	as FPDF	# IF NECESSARY, INSTALL WITH: $ pip install fpdf

class PdfWriter:
	'''
	Description: Class to simplify creating PDF-files.
	-            Can be used as it is, or be customized by inheriting.
	Uses       : fpdf; if necessary, install with: $ pip install fpdf
	-            PIL ; if necessary, install with: $ pip install pillow
	Changes    : 06-12-2021 Created
	-            08-12-2021 Added alignments for fixed blocks.
	-            09-12-2021 Added text_height() and changed default factor to 1.5.
	-            10-12-2021 Added encoding for all text.
	-            26-04-2022 Added image-function.
	Version    : 20220426A
	'''

	#region ----- DOCUMENT -----

	def __init__(self, path, landscape=False, margin_left=10, margin_top=10, margin_right=10):
		'''
		path: Will be overwritten silently when finishing the pdf.
		'''
		self.DEVELOPING	= False

		# INPUT/CHECKS:
		self.path		= str(path)
		self.landscape	= bool(landscape)
		margin_left		= float(margin_left)
		margin_top		= float(margin_top)
		margin_right	= float(margin_right)
		if not path:
			raise ValueError('The path for the new PDF-file is mandatory')

		# NEW DOCUMENT WITH DEFAULTS:
		orientation	= 'L' if self.landscape else 'P'
		self.pdf	= FPDF.FPDF(orientation=orientation, unit='mm', format='A4')
		self.margins(margin_left, margin_top, margin_right, make_default=True)
		self.font_times(size=12, make_default=True)
		self.text_height(factor=1.5, make_default=True)
		self.color_text(0, 0, 0, make_default=True)
		self.color_draw(0, 0, 0, make_default=True)
		self.color_fill(255, 255, 255, make_default=True)
		self.line_width(1, make_default=True)
		self.pdf.add_page()

	def properties(self, title=None, subject=None, keywords=None, author=None, creator=None):
		'''
		To add some metadata to the document-properties.
		keywords: To be supplied in a single string.
		'''
		if title != None:
			self.pdf.set_title(str(title))
		if subject != None:
			self.pdf.set_subject(str(subject))
		if keywords != None:
			self.pdf.set_keywords(str(keywords))
		if author != None:
			self.pdf.set_author(str(author))
		if creator != None:
			self.pdf.set_creator(str(creator))

	def finish(self):
		'''Creates the physical file.'''
		self.pdf.output(self.path)
		print('Created PDF:', self.path)

	#endregion

	#region ----- PAGE -----

	def use_footer(self):
		self.pdf.footer = self.footer

	def footer(self):
		'''No function to be used directly. It must be registered by use_footer().'''
		import os
		import datetime
		self.pdf.set_y(-15)
		self.pdf.set_font('Arial', 'I', 8);
		dtm			= datetime.datetime.now()
		text_left	= '%s      %s' % (os.path.basename(self.path), dtm.strftime('%Y-%m-%d %H:%M'))
		text_right	= 'Page %i' % self.pdf.page_no()
		self.pdf.cell(90, 10, txt=text_left, align='L')
		self.pdf.cell(0, 10, txt=text_right, align='R')

	def new_page(self):
		'''If the new page should have other margins than the previous one, you better call margins() first!'''
		self.pdf.add_page()

	def background_image(self, file, x, y, width, height):
		x		= float(x)
		y		= float(y)
		width	= float(width)
		height	= float(height)
		self.pdf.image(file, x, y, width, height)

	def margins(self, left, top, right, make_default=False):
		'''
		Default is 10mm.
		Better use this function before starting a new page, else the 1st line will have the old left-margin!
		'''
		left			= float(left)
		top				= float(top)
		right			= float(right)
		make_default	= bool(make_default)
		self.pdf.set_margins(left, top, right=right)
		if make_default:
			self.margin_left	= left
			self.margin_top		= top
			self.margin_right	= right

	def margins_default(self):
		self.pdf.set_margins(self.margin_left, self.margin_top, self.margin_right)

	def line_width(self, w, make_default=False):
		'''This is the width of drawn lines, not text-lines!'''
		w = float(w)
		w = 1 if w < 0 else w
		self.pdf.set_line_width(w)
		if make_default:
			self.draw_line_width = w

	def line_width_default(self):
		'''Restores the default line-width for drawing. Useful after temporarily having changed it.'''
		self.line_width(self.drawn_line_width, make_default=False)

	#endregion

	#region ----- COLORS -----

	def color_draw(self, r, g, b, make_default=False):
		r	= int(r)
		g	= int(g)
		b	= int(b)
		r 	= 0 if r < 0 else (255 if r > 255 else r)
		g 	= 0 if g < 0 else (255 if g > 255 else g)
		b 	= 0 if b < 0 else (255 if b > 255 else b)
		self.pdf.set_draw_color(r, g, b)
		if make_default:
			self.color_draw_r = r
			self.color_draw_g = g
			self.color_draw_b = b

	def color_draw_default(self):
		'''Restores the default color for drawing. Useful after temporarily having used another color.'''
		self.pdf.set_draw_color(self.color_draw_r, self.color_draw_g, self.color_draw_b)

	def color_fill(self, r, g, b, make_default=False):
		r	= int(r)
		g	= int(g)
		b	= int(b)
		r 	= 0 if r < 0 else (255 if r > 255 else r)
		g 	= 0 if g < 0 else (255 if g > 255 else g)
		b 	= 0 if b < 0 else (255 if b > 255 else b)
		self.pdf.set_fill_color(r, g, b)
		if make_default:
			self.color_fill_r = r
			self.color_fill_g = g
			self.color_fill_b = b

	def color_fill_default(self):
		'''Restores the default color for filling. Useful after temporarily having used another color.'''
		self.pdf.set_fill_color(self.color_fill_r, self.color_fill_g, self.color_fill_b)

	def color_text(self, r, g, b, make_default=False):
		r	= int(r)
		g	= int(g)
		b	= int(b)
		r 	= 0 if r < 0 else (255 if r > 255 else r)
		g 	= 0 if g < 0 else (255 if g > 255 else g)
		b 	= 0 if b < 0 else (255 if b > 255 else b)
		self.pdf.set_text_color(r, g, b)
		if make_default:
			self.color_text_r = r
			self.color_text_g = g
			self.color_text_b = b

	def color_text_default(self):
		'''Restores the default color for text. Useful after temporarily having used another color.'''
		self.pdf.set_text_color(self.color_text_r, self.color_text_g, self.color_text_b)

	#endregion

	#region ----- FONTS -----

	def __font(self, name, size, bold, italic, underlined, make_default):
		name			= str(name)
		size 			= int(size)
		bold			= bool(bold)
		italic			= bool(italic)
		underlined		= bool(underlined)
		make_default	= bool(make_default)

		if size < 1:
			size = 12
		decoration 	= ''
		decoration += 'B' if bold else ''
		decoration += 'I' if italic else ''
		decoration += 'U' if underlined else ''
		self.pdf.set_font(name, decoration, size)

		if make_default:
			self.font_name			= name
			self.font_size			= size
			self.font_bold			= bold
			self.font_italic		= italic
			self.font_underlined	= underlined

	def font_default(self):
		'''Restores the default font for the document.'''
		self.__font(self.font_name, self.font_size, self.font_bold, self.font_italic, self.font_underlined, make_default=False)

	def font_arial(self, size=12, bold=False, italic=False, underlined=False, make_default=False):
		'''Size in points.'''
		self.__font('Arial', size, bold, italic, underlined, make_default)

	def font_courier(self, size=12, bold=False, italic=False, underlined=False, make_default=False):
		'''Size in points.'''
		self.__font('Courier', size, bold, italic, underlined, make_default)

	def font_symbol(self, size=12, bold=False, italic=False, underlined=False, make_default=False):
		'''Size in points. (This font uses Greek letters.)'''
		self.__font('Symbol', size, bold, italic, underlined, make_default)

	def font_times(self, size=12, bold=False, italic=False, underlined=False, make_default=False):
		'''Size in points.'''
		self.__font('Times', size, bold, italic, underlined, make_default)

	def font_zapf(self, size=12, bold=False, italic=False, underlined=False, make_default=False):
		'''Size in points. (This font uses weird symbols, the meaning of which is??)'''
		self.__font('ZapfDingbats', size, bold, italic, underlined, make_default)

	def text_height(self, factor=-1, make_default=False):
		'''
		Returns and/or changes the current font-size x factor.
		If a factor < 0 is given, then the default-factor will be taken, else the given factor.
		And in the latter case it even can be made the new default.
		'''
		factor = float(factor)
		if factor < 0:
			return self.pdf.font_size * self.text_height_factor
		else:
			if make_default:
				self.text_height_factor = factor
			return self.pdf.font_size * factor

	#endregion

	#region ----- COPIED LIBRARY-FUNCTIONS -----

	def convert(units:float, type:str) -> float:
		'''
		Description: To convert all sorts of data into other units (tobe extended regularly.)
		Parameters : type: supported types are:
		-            - "pixel_millimeter" and "millimeter_pixel"
		Links      : https://www.unitconverters.net/
		Changes    : 26-04-2022 Created
		Version    : 20220426A
		'''
		units	= float(units)
		type	= str(type)

		if type == "pixel_millimeter":
			return units * 0.2645833333333
		elif type == "millimeter_pixel":
			return units / 0.2645833333333
		else:
			raise ValueError("Type not supported: '%s'" % type)

	def image_size(path:str):
		'''
		Description: To establish the size in pixels of an image-file.
		Uses       : Pillow (PIL); if not installed: pip3 install pillow
		Returnvalue: A tuple with width and height.
		Changes    : 26-04-2022 Created
		Version    : 20220426A
		'''
		import os
		import PIL
		from PIL import Image

		# CHECKS:
		path = str(path)
		if not os.path.isfile(path):
			raise ValueError("The path does not exist: '%s'" % path)
		# (WE ASSUME HERE THAT THE FILE REALLY CONTAINS IMAGE-DATA)

		img = Image.open(path)
		return img.size

	# FUNCTION BELOW IS NECESSARY FOR ALL TEXT-OUTPUT, BECAUSE THE BUILTIN FPDF-FONTS DO NOT SUPPORT ALL UNICODES!

	def replace_illegals(text, encoding, replace=True):
		'''
		Description: Replaces Unicode-chars that a certain encoding does not accept.
		Parameters : encoding: The regular Python-encodings, like 'iso8859_1'.
		-            replace : When True, puts a '?' instead of the illegal letter; when False, skips the letter.
		Remarks    : Initially created for the builtin fonts in fpdf, that do not support all Unicode chars.
		Example    : replace_illegals('female:♀ male:♂ currencies:$€£¥', 'iso8859_1') --> 'female:? male:? currencies:$?£¥'
		-            replace_illegals('...idem...', 'iso8859_1', replace=False)       --> 'female: male: currencies:$£¥'
		Changes    : 09-12-2021 Created
		Version    : 20211209A
		'''
		replace = bool(replace)
		if text != None:
			text = str(text)
			if replace:
				bytes = text.encode(encoding, errors='replace')
			else:
				bytes = text.encode(encoding, errors='ignore')
			text  = bytes.decode(encoding) # ILLEGAL CHARS PERHAPS ARE REPLACED WITH '?'.
		return text

	REPLACE_ENCODING = 'iso8859_1'

	def encode(self, text):
		'''To simplify the use of the function replace_illegals() above.'''
		if text == None:
			return ''
		else:
			return PdfWriter.replace_illegals(text, 'iso8859_1', replace=True)

	#endregion

	#region ----- WRITING -----

	def new_line(self, count=1, height=0):
		'''
		height:
		When = 0 then the previous line-height will taken as default.
		When < 0 then calculated will be a default (1.5 * current font-size).
		When > 0 then that value will be used.
		'''
		count = int(count)
		height = float(height)
		if height == 0:
			while count > 0:
				self.pdf.ln()
				count -= 1
		else:
			if height < 0:
				height = self.text_height()
			while count > 0:
				self.pdf.ln(height)
				count -= 1

	def text(self, text):
		'''
		Adds text in the current font and does not end with a newline.
		'''
		text = self.encode(text)
		self.pdf.write(self.text_height(), txt=text)

	def text_line(self, text):
		'''
		Adds text in the current font and finishes with a newline.
		'''
		self.text(text)
		self.new_line()

	def fixed_block(self, width, height, text, border=False, newline=False, fill=False, align='left'):
		'''
		Adds a block with text vertical-aligned in the middle.
		width/height: Floating point in mm.
		When the block is too small, then the text just walks out of it, so no wrapping or truncation.
		So, contrary to auto_block() better suited for title-blocks etc.
		align: 'left', 'right' or 'center'.
		'''
		width	= float(width)
		height	= float(height)
		text	= self.encode(text)
		border	= bool(border)
		newline	= bool(newline)
		fill	= bool(fill)
		align	= str(align)
		ln		= 1 if newline else 0
		if align == 'right':
			align = 'R'
		elif align == 'center':
			align = 'C'
		else:
			aling = 'L'
		self.pdf.cell(width, height, text, border=border, ln=ln, fill=fill, align=align)

	def auto_block(self, width, height, text, border=False, newline=False, fill=False):
		'''
		Adds a block with text vertical-aligned in the middle.
		width/height: Floating point in mm.
		When the block is too small, then the line breaks automatically and the block grows.
		Also after every newline a new block will be added.
		So, contrary to fixed_block() better suited for blocks with ongoing text.
		'''
		width	= float(width)
		height	= float(height)
		text	= self.encode(text)
		border	= bool(border)
		newline	= bool(newline)
		fill	= bool(fill)
		# ln		= 1 if newline else 0   # VS-CODE SHOWS A ln-PARAMETER, BUT IT GIVES A RUNTIME-ERROR.
		self.pdf.multi_cell(width, height, text, border=border, fill=fill)

	def indent(self, width):
		width	= float(width)
		self.pdf.cell(width)

	def draw_line(self, x1, y1, x2, y2):
		'''Draws a line with the current line-width and draw color.'''
		x1 = float(x1)
		y1 = float(y1)
		x2 = float(x2)
		y2 = float(y2)
		self.pdf.line(x1, y1, x2, y2)

	def link(self, text, url):
		'''Creates an external hyperlink.'''
		text = str(text)
		url  = str(url)
		self.pdf.set_text_color(0, 0, 255)
		self.pdf.write(self.text_height(), txt=text, link=url)
		self.color_text_default()

	#endregion

	#region ----- IMAGES/DRAWING -----

	def image(self, file, x=None, y=None, width=0, height=0, absolute=True):
		'''
		x, y         : If omitted, the image will be placed in the next textblock, as one would expect.
		width, height: In millimeters!
		-              If omitted, the image will be rendered at its original size.
		-              When the image is too large, it will get its own page, and part of the image will drop off this page.
		absolute     : If True,  then width and height will be taken literally.
		-              If False, then width and height will be considered max-boundaries.
		'''
		if x != None:
			x		= float(x)
		if y != None:
			y		= float(y)
		width		= float(width)
		height		= float(height)
		absolute	= bool(absolute)

		if absolute:
			self.pdf.image(file, x, y, width, height)
		else:
			# WE HAVE DETERMINE THE IMAGE-SIZE, AND RESIZE IT IF NECESSARY:
			pix_width, pix_height	= PdfWriter.image_size(file)
			mm_width				= PdfWriter.convert(pix_width,  "pixel_millimeter")
			mm_height				= PdfWriter.convert(pix_height, "pixel_millimeter")
			factor_width			= width / mm_width
			factor_height			= height / mm_height
			if self.DEVELOPING:
				print("Pixels: %f, %f" % (pix_width, pix_height))
				print("Millimeters: %f, %f" % (mm_width, mm_height))
				print("Factors: %f, %f" % (factor_width, factor_height))

			if factor_width >= 1 and factor_height >= 1:
				# FITTING, SO DO NOTHING SPECIAL:
				if self.DEVELOPING:
					print('Normal')
				self.pdf.image(file, x, y, width, height)
			elif factor_width < factor_height:
				if self.DEVELOPING:
					print('Width')
				self.pdf.image(file, x, y, mm_width*factor_width, 0)
			else:
				if self.DEVELOPING:
					print('Height')
				self.pdf.image(file, x, y, 0, mm_height*factor_height)

	#endregion
