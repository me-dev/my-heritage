'''
File with print-functions to be imported by scripts, mainly for colored Usage-text.
Last version: 20210116A
'''

def blue(text, end='\n'):
	text = str(text)
	print('\x1b[1;96m%s\x1b[0m' % text, end=end)

def green(text, end='\n'):
	text = str(text)
	print('\x1b[1;92m%s\x1b[0m' % text, end=end)

def red(text, end='\n'):
	text = str(text)
	print('\x1b[1;91m%s\x1b[0m' % text, end=end)

def yellow(text, end='\n'):
	text = str(text)
	print('\x1b[1;93m%s\x1b[0m' % text, end=end)

