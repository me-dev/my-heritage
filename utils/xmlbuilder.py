import xml.etree.ElementTree as XET
from xml.dom import minidom

class XmlBuilder:
	'''
	Description: A class for simplifying creating XML-files, for instance for data-exports.
	Example    : xb = XmlBuilder('person')
	-            xb.root_child('born')
	-            xb.previous_child('place', text='Amsterdam')
	-            xb.previous_sibling('date', text='1980-02-29')
    -            print(xb)
	Changes    : 04-12-2021 Created
	-          : 05-12-2021 Added escaping with correct_tagname().
	Version    : 20211205A
	'''

	def __init__(self, root='root'):
		self.root		= XET.Element(str(root))
		self.previous	= self.root
		self.parent		= None
		# self.tree = ElementTree(self.root)

	def __str__(self):
		bytes    = XET.tostring(self.root, 'UTF-8')
		reparsed = minidom.parseString(bytes)
		return reparsed.toprettyxml(indent="  ")

	def correct_tagname(self, tagname):
		'''
		Removes all illegal characters from a tagname.
		If nothing remains to return, an error will be raised.
		For simplicity we only consider A-Z and a-z to be valid letters.
		'''
		result = ''
		for c in tagname:
			if result == '':
				if c in '_ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz':
					result += c
			elif c in '_ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-0123456789':
				result += c
		if result == '':
			raise ValueError("The following tagname could not be corrected into a valid result: '%s'" % tagname)
		else:
			return result

	def new_child(self, parent, tagname, text=None):
		'''
		Creates a new element and returns it.
		If the parent is None, then the root-node will be taken.
		As a side-effect .parent and .previous will be updated.
		'''
		self.parent	= parent if parent != None else self.root
		tagname		= self.correct_tagname(tagname)		# (TAGNAMES ARE NOT ESCAPED AUTOMATICALLY)
		child		= XET.Element(str(tagname))
		if (text):
			child.text = str(text)						# (TEXT-NODES ARE ESCAPED AS EXPECTED)
		self.parent.append(child)
		self.previous = child
		return child

	def root_child(self, tagname, text=None):
		'''Adds a new child to the root-element and returns it.'''
		return self.new_child(self.root, tagname, text)

	def previous_child(self, tagname, text=None):
		'''Adds a new child to the previously created element and returns it.'''
		return self.new_child(self.previous, tagname, text)

	def previous_sibling(self, tagname, text=None):
		'''Adds a new sibling to the previously created element and returns it.'''
		return self.new_child(self.parent, tagname, text)
