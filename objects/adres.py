from utils.metadata 	import Metadata
from objects.entiteit	import Entiteit

class Adres(Entiteit):
	'''
	De wrapper-class voor de records die betrekking hebben op adressen (RESI: email, woonplaats).
	Wordt weinig gebruikt en is ook slecht gevuld.
	'''

	def __init__(self, app, record0):
		classname = self.__class__.__name__
		if not classname in app.metadata:
			app.metadata[classname] = Metadata([
				# PROPERTY      	NUM	TYPE	CONTEXT
				('_regelnummer',	1,	'int',	''),				# VERPLICHT
				('_recordnummer',	1,	'int',	''),				# VERPLICHT
				('_id',				1,	'str',	'RESI'),			# VERPLICHT (id OF_id)
				('_uid',			1,	'str',	'RESI/_UID'),
				('_rin',			1,	'str',	'RESI/RIN'),
				('type',			1,	'str',	'RESI/TYPE'),
				('adres',			1,	'str',	'RESI/ADDR'),
				('adres',			1,	'str',	'RESI/ADDR/ADR1'),
				('adres',			1,	'str',	'RESI/ADDR/ADR2'),
				('emailadres',		1,	'str',	'RESI/EMAIL'),
				('datum',			1,	'str',	'RESI/DATE'),
			])
		super().__init__(app, record0, app.metadata[classname])
		self.verwerk(record0)
