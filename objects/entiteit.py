import utils.metadata
import utils.object_functions		as OF
import utils.filesystem_functions	as FS
import utils.text_functions			as TF
import utils.textbuilder 			as TB
import utils.htmlbuilder 			as HB
import utils.xmlbuilder				as XB
import utils.pdfwriter				as PW

class Entiteit:
	'''
	De base-class voor alle andere hoofd- en sub-entiteiten.
	Bevat nu bijna alle logica, terwijl de sub-classes vaak niet meer dan metadata defnieren.
	'''

	def __init__(self, app, record0, metadata=None):
		'''De base-constructor moet aangeroepen worden, voor het huishoudelijke werk.'''
		self._classname				= self.__class__.__name__
		self._app					= app
		self._records				= []
		self._offset				= record0.level		# SOMMIGE TAGS (BIJV. OBJE) KOMEN VAKER VOOR; WE VERWIJDEREN LATER ALLES ERVOOR.
		self._tags					= []				# ONTHOUD WAAR WE ZIJN IN EEN ENTITEIT, BIJV. ['INDI', 'NAME', ´SURN']
		self._context				= None				# IS DE KETEN VAN TAGS, BIJV.: 'INDI/NAME/SURN'
		self._current_child			= None				# ONTHOUDT OF WE BEZIG ZIJN MET DE RECORDS VOOR EEN CHILD-OBJECT.
		self._current_child_context = None

		if metadata:
			for rule in metadata.rules:
				if rule.number == 1:
					setattr(self, rule.prop, None)
				else:
					setattr(self, rule.prop, [])

		# DE VOLGENDE PROPERTIES STAAN IN DE METADATA, OM ZE MEE TE LATEN LOPEN MET VERBOSE PRINTS E.D.
		self._regelnummer	= record0.regelnummer	# VAN DE REGEL IN DE TEKST (DAARIN WORDEN ZOWEL CRLF ALS LF GEBRUIKT).
		self._recordnummer	= record0.recordnummer	# VAN HET RECORD, DAT DOOR CRLF AFGESLOTEN WORDT.

	def _print_metadata(self, metadata):
		'''(Voor debuggen)'''
		for rule in metadata.rules:
			print("%-30s %-5s %-20s %s" % (rule.prop, rule.number, rule.type, rule.context))

	def __repr__(self):
		if self.id:
			return "Entiteit '%s' %s (regel %s, record %s)" % (self._classname, self.id, self._regelnummer, self._recordnummer)
		else:
			return "Entiteit '%s' (id?) regel %s, record %s" % (self._classname, self._regelnummer, self._recordnummer)

	def __str__(self):
		return self.naar_tekst()

	def verwerk(self, record):
		'''Subclasses moeten deze als eerste aanroepen: super.verwerk(...)'''

		# BEWAAR RECORDS VOOR EVT. DEBUGGEN E.D.:
		self._records.append(record)

		# BEPAAL DE CONTEXT (SOMMIGE LABELS BESTAAN OP VERSCHILLENDE PLAATSEN:
		del self._tags[record.level-self._offset:]		# VERWIJDERT VOORGAANDE CONTEXT VAN HETZELFDE OF LAGER NIVEAU.
		self._tags.append(record.label)					# VOEGT DE EIGEN CONTEXT TOE.
		self._context = '/'.join(self._tags)			# EN MAAKT HET MAKKELIJKER IN HET GEBRUIK.

		# ALS WE BEZIG ZIJN MET RECORDS VOOR EEN CHILD-OBJECT, DELEGEER DIE DAN:
		if self._current_child and self._context.startswith(self._current_child_context):
			self._current_child.verwerk(record)
			return

		# NEE, DAN RESET CHILD ...:
		self._current_child 		= None
		self._current_child_context	= None

		# ... EN ZOEK IN DE METADATA EEN MATCHENDE CONTEXT:
		for rule in self._app.metadata[self._classname].rules:

			# JUISTE METADATA?
			if rule.context == self._context:

				# CONVERTEER DE WAARDE:
				value = None
				if rule.type in ['str', 'str+', 'html', 'html+', 'url']:
					value = str(record.tekst) if record.tekst else ''
				elif rule.type == 'int':
					value = int(record.tekst)
				else:
					value = OF.instantiate(rule.type, self._app, record)
					self._current_child			= value
					self._current_child_context = rule.context + '/'

				# WAAR MOETEN WE NAAR TOE MET DIE WAARDE?
				old_value = getattr(self, rule.prop)
				if old_value == None:
					old_value = ''
				if rule.number == 1:
					if rule.type == 'str+':
						setattr(self, rule.prop, old_value + '\n' + value)
					elif rule.type == 'html+':
						setattr(self, rule.prop, old_value + value)
					elif old_value and old_value != value and self._app.modus >= 2:
						print("Eigenschap '%s' van entiteit '%s' heeft al een waarde; context: '%s' (regel %i)" % (rule.prop, self._classname, self._context, record.regelnummer))
						print("Old value: '%s'" % old_value)
						print("New value: '%s'" % value)
						if rule.prop.startswith('_'):
							print("(te negeren)")
						print()
						'''
						if rule.prop.startswith('_'):
							print("Eigenschap '%s' van entiteit '%s' heeft al een waarde; context: '%s' (regel %i)" % (rule.prop, self._classname, self._context, record.regelnummer))
						else:
							raise Exception("Eigenschap '%s' van entiteit '%s' heeft al een waarde; context: '%s' (regel %i)" % (rule.prop, self._classname, self._context, record.regelnummer))
						'''
					else:
						setattr(self, rule.prop, value)
				else:
					old_value.append(value)
				return

		# ALS WE HIER KOMEN IS DE CONTEXT NIET HERKEND:
		if self._app.modus >= 2:
			raise Exception("Entiteit '%s' ondersteunt nog niet de volgende context: '%s' (regel %i)" % (self._classname, self._context, record.regelnummer))

	def vul_verwijzingen(self, dic_ids):
		'''Om, nadat alles is ingelezen, m.b.v. de ID's verwijzingen naar andere entiteiten te vullen met objecten.'''
		pass

	def kop(self):
		'''
		Te overriden door sub-classes met een voor hun zinvolle kop.
		Deze base-implementatie geeft alleen de classname.
		'''
		return self._classname

#region ----- EXPORT TEKST -----

	def naar_tekst(self, tabs=0, fragment=False):
		'''
		Om in de shell te printen.
		Maakt gebruik van 3 sub-functies, die zonodig door een sub-class anders geimplementeerd kunnen worden.
		'''
		tb = TB.TextBuilder(tab='    ', tabs=tabs)
		self.naar_tekst_kop(tb, tabs, fragment)
		self.naar_tekst_rules(tb, tabs)
		return tb.text

	def naar_tekst_kop(self, tb, tabs, fragment):
		'''Afgesplitst van naar_tekst() zodat een subclass zonodig sommige rules op een eigen manier kan doen en de kop standaard.'''
		tb.line(tabs, self.kop())
		if not fragment:
			tb.rule(len(self.kop()), tabs=tabs)

	def naar_tekst_rules(self, tb, tabs):
		'''Afgesplitst van naar_tekst() zodat een subclass zonodig sommige rules op een eigen manier kan doen en andere standaard.'''
		done = set()
		for rule in self._app.metadata[self._classname].rules:
			self.naar_tekst_rule(rule, tb, tabs, done)

	def naar_tekst_rule(self, rule, tb, tabs, done):
		'''Afgesplitst van naar_tekst() zodat een subclass zonodig sommige rules op een eigen manier kan doen en andere standaard.'''
		# CHECKS:
		if rule.prop in done:
			return
		if rule.prop.startswith('_') and not self._app.verbose:
			return

		# QUICK-AND-DIRTY; WE MAKEN VAN ALLES EEN LIST:
		if rule.number == 1:
			lst = [getattr(self, rule.prop)]
		else:
			lst = getattr(self, rule.prop)

		for item in lst:
			if rule.type in ['str', 'str+', 'url', 'int']:
				tb.prop(tabs+1, rule.caption, item)
			elif rule.type in ['html', 'html+']:
				# print('|%s|' % item)
				tb.prop(tabs+1, rule.caption, TF.html_decode(item, remove_tags=True))
			else:
				# EEN CHILD-OBJECT:
				if item == None:
					tb.prop(tabs+1, rule.caption, None)
				else:
					tb.add(item.naar_tekst(tabs=tabs+1, fragment=True))
		done.add(rule.prop)

#endregion

#region ----- EXPORT HTML -----

	def naar_html(self, template=None, fragment=True):
		'''
		Deze base-class-functie levert altijd een fragment op met eigenschappen (in de vorm van table-rows).
		Een sub-class kan deze logica gebruiken om de inhoud voor een template te krijgen.
		De parameters template en fragment worden daarom niet hier gebruikt, maar laten zien wat een sub-class moet implementeren.
		'''

		if not self._classname in self._app.metadata:
			if self._app.modus >= 2:
				raise Exception("Entiteit '%s' heeft nog geen metatadata voor naar_html()." % self._classname)
			else:
				return ''

		hb = HB.HtmlBuilder(fragment=True)
		hb.fragment.add_tr_headers(self.kop(), '')
		self.naar_html_rules(hb)
		return hb.to_html(tab='')

	def naar_html_rules(self, hb):
		'''Afgesplitst van naar_html() om sub-classes gelegenheid te geven te overriden.'''
		done = set()		# ONTHOUDT WELKE GEDAAN ZIJN (SOMMIGE PROPS HEBBEN MEERDERE CONTEXTS)
		for rule in self._app.metadata[self._classname].rules:
			self.naar_html_rule(rule, hb, done)

	def naar_html_rule(self, rule, hb, done):
		'''Afgesplitst van naar_html() om sub-classes gelegenheid te geven te overriden.'''

		# CHECKS:
		if rule.prop in done:
			return
		if rule.prop.startswith('_') and not self._app.verbose:
			return

		# QUICK-AND-DIRTY; WE MAKEN VAN ALLES EEN LIST:
		value = getattr(self, rule.prop)
		if rule.number == 1:
			lst = [value]
		else:
			lst = value

		for item in lst:
			if rule.type in ['str', 'str+', 'int']:
				hb.fragment.add_tr_property(rule.caption, item, skip_none=(not self._app.verbose), escape=True)
			elif rule.type in ['html', 'html+']:
				# MERKWAARDIG IS DAT HTML-TEKST IN MY-HERITAGE SOMS DUBBEL ESCAPED IS!
				if item != None and '&amp;lt;br&amp;gt;' in item:
					item = TF.html_decode(item)
				if item != None and '&lt;br&gt;' in item:
					item = TF.html_decode(item)
				hb.fragment.add_tr_property(rule.caption, item, skip_none=(not self._app.verbose), escape=False)
			elif rule.type == 'url':
				hb.fragment.add_tr_proplink(rule.caption, item, item, target='_blank')
				if item != None:
					item = str(item)
					pos  = item.rfind('.')
					if pos > -1 and FS.extension_image(item[pos:]):
						hb.fragment.add_tr_propimg('', item, attributes=[('width', '20%'), ('height', '20%')])
			else:
				# EEN CHILD-OBJECT:
				if item == None:
					if self._app.verbose:
						hb.fragment.add_tr_property(rule.caption, None)
				else:
					hb.fragment.add_literal(item.naar_html(template=None, fragment=True))

		done.add(rule.prop)

#endregion

#region ----- EXPORT JSON -----

	def naar_json(self, tabs=0):
		TAB		= '  '
		indent0 = TAB * tabs
		indent1 = indent0 + TAB

		sb = TF.StandardStringBuilder()
		sb.addline('%s{'				% indent0)
		sb.addline('%s"class": "%s",'	% (indent1, self._classname))
		self.naar_json_rules(sb, tabs+1)
		sb.add('\n%s}'					% indent0)
		sb.close()
		return sb.text()

	def naar_json_rules(self, sb, tabs):
		'''Afgesplitst van naar_json() om sub-classes gelegenheid te geven te overriden.'''
		first	= True
		done 	= set()		# ONTHOUDT WELKE GEDAAN ZIJN (SOMMIGE PROPS HEBBEN MEERDERE CONTEXTS)
		for rule in self._app.metadata[self._classname].rules:
			# CHECKS:
			if rule.prop in done:
				continue
			done.add(rule.prop)
			if rule.prop.startswith('_') and not self._app.verbose:
				continue

			# LEVERT DE RULE TEKST OP?
			part = self.naar_json_rule(rule, tabs)
			if part:
				if not first:
					sb.addline(",")
				first = False
				sb.add(part)

	def naar_json_rule(self, rule, tabs):
		'''
		Afgesplitst van naar_json() om sub-classes gelegenheid te geven te overriden.
		Deze functie moet tekst of None teruggeven, waarna de caller al dan niet concateneert.
		(Laat de tekst niet eindigen met een newline.)
		'''
		TAB			= '  '
		indent0		= TAB * tabs
		indent1		= indent0 + TAB
		item_indent	= ''

		# PROLOOG:
		part = '%s"%s": ' % (indent0, rule.prop)
		if rule.number > 1:
			part += "[\n"
			item_indent = indent1

		# QUICK-AND-DIRTY; WE MAKEN VAN ALLES EEN LIST:
		value = getattr(self, rule.prop)
		if rule.number == 1:
			lst = [value]
		else:
			lst = value

		first = True
		count = 0
		for item in lst:
			if item == None or item == '':
				continue

			if not first:
				part += ',\n'
			first = False

			if rule.type in ['str', 'str+', 'url']:
				part += '%s"%s"' % (item_indent, item.replace('"', '\''))
				count += 1
			elif rule.type in ['int']:
				part += '%s%i' % (item_indent, item)
				count += 1
			elif rule.type in ['html', 'html+']:
				# MERKWAARDIG IS DAT HTML-TEKST IN MY-HERITAGE SOMS DUBBEL ESCAPED IS!
				if item != None and '&amp;lt;br&amp;gt;' in item:
					item = TF.html_decode(item)
				if item != None and '&lt;br&gt;' in item:
					item = TF.html_decode(item)
				part += '%s"%s"' % (item_indent, item.replace('"', '\''))
				count += 1
			else:
				# EEN CHILD-OBJECT:
				if rule.type in ['Persoon', 'Familie', 'Bron']:
					return None
				#elif rule.type in ['Bestand']:
				#	part += item.naar_json(tabs=tabs+1)
				#	count += 1
				else:
					# return None
					part += item.naar_json(tabs=tabs+1)
					count += 1

		# EPILOOG:
		if rule.number > 1:
			part += "\n%s]" % indent0

		if count == 0:
			return None
		else:
			return part

#endregion

#region ----- EXPORT XML -----

	def naar_xml(self, xb=None, node_parent=None):
		is_root_object = False
		if xb == None:
			is_root_object = True
			rootname = self._classname.lower()
			xb = XB.XmlBuilder(root=rootname)
			node_parent = xb.root

		self.naar_xml_rules(xb, node_parent)
		return str(xb) if is_root_object else ''

	def naar_xml_rules(self, xb, node_parent):
		'''Afgesplitst van naar_xml() om sub-classes gelegenheid te geven te overriden.'''
		done = set()		# ONTHOUDT WELKE GEDAAN ZIJN (SOMMIGE PROPS HEBBEN MEERDERE CONTEXTS)
		for rule in self._app.metadata[self._classname].rules:
			# CHECKS:
			if rule.prop in done:
				continue
			done.add(rule.prop)
			if rule.prop.startswith('_') and not self._app.verbose:
				continue

			self.naar_xml_rule(xb, node_parent, rule)

	def naar_xml_rule(self, xb, node_parent, rule):
		'''
		Afgesplitst van naar_xml() om sub-classes gelegenheid te geven te overriden.
		'''

		# QUICK-AND-DIRTY; WE MAKEN VAN ALLES EEN LIST:
		value = getattr(self, rule.prop)
		if rule.number == 1:
			lst = [value]
		else:
			lst = value

		node_children = None	# ONLY NEEDED WHEN THERE ARE MORE CHILD-OBJECTS.

		for item in lst:
			if item == None or item == '':
				continue

			if rule.type in ['str', 'str+', 'url', 'int']:
				xb.new_child(node_parent, rule.prop, text=item)
			elif rule.type in ['html', 'html+']:
				# MERKWAARDIG IS DAT HTML-TEKST IN MY-HERITAGE SOMS DUBBEL ESCAPED IS!
				if item != None and '&amp;lt;br&amp;gt;' in item:
					item = TF.html_decode(item)
				if item != None and '&lt;br&gt;' in item:
					item = TF.html_decode(item)
				xb.new_child(node_parent, rule.prop, text=item)
			else:
				# EEN CHILD-OBJECT:
				if rule.type in ['Persoon', 'Familie', 'Bron']:
					return None
				else:
					if rule.number == 1:
						node_child = xb.new_child(node_parent, rule.prop)
					else:
						if node_children == None:
							node_children = xb.new_child(node_parent, rule.prop)
						node_child = xb.new_child(node_children, 'item')
					item.naar_xml(xb, node_child)

#endregion

#region ----- EXPORT PDF -----

	def naar_pdf(self, path, pw=None):
		'''
		pw: pdf-writer
		'''
		is_root_object = False
		if pw == None:
			is_root_object = True
			pw = PW.PdfWriter(path)
			pw.use_footer()
			pw.color_fill(224, 224, 224, make_default=True)
			pw.fixed_block(180, 15, self.kop(), border=True, newline=True, fill=True, align='center')
			pw.new_line(height=-1)

		self.naar_pdf_rules(pw)

		if is_root_object:
			pw.finish()

	def naar_pdf_rules(self, pw):
		'''
		Afgesplitst van naar_xml() om sub-classes gelegenheid te geven te overriden.
		pw: pdf-writer
		'''
		done = set()		# ONTHOUDT WELKE GEDAAN ZIJN (SOMMIGE PROPS HEBBEN MEERDERE CONTEXTS)
		for rule in self._app.metadata[self._classname].rules:
			# CHECKS:
			if rule.prop in done:
				continue
			done.add(rule.prop)
			if rule.prop.startswith('_') and not self._app.verbose:
				continue

			self.naar_pdf_rule(pw, rule)

	def naar_pdf_rule(self, pw, rule):
		'''
		Afgesplitst van naar_pdf() om sub-classes gelegenheid te geven te overriden.
		pw: pdf-writer
		'''

		# QUICK-AND-DIRTY; WE MAKEN VAN ALLES EEN LIST:
		value = getattr(self, rule.prop)
		if rule.number == 1:
			lst = [value]
		else:
			lst = value

		index = 0
		for item in lst:
			if item == None or item == '':
				continue

			count  = len(lst)
			index += 1

			if rule.type == 'url':
				self._pdf_prop(pw, rule.prop, item, is_url=True)
			elif rule.type in ['str', 'str+', 'int']:
				self._pdf_prop(pw, rule.prop, item)
			elif rule.type in ['html', 'html+']:
				# MERKWAARDIG IS DAT HTML-TEKST IN MY-HERITAGE SOMS DUBBEL ESCAPED IS!
				if item != None:
					item = TF.html_decode(item, remove_tags=True, insert_newlines=True)
				if item != None and '&amp;lt;br&amp;gt;' in item:
					item = TF.html_decode(item, remove_tags=True, insert_newlines=True)
				if item != None and '&lt;br&gt;' in item:
					item = TF.html_decode(item, remove_tags=True, insert_newlines=True)
				# THE BUILTIN FONTS OF FPDF DONT SUPPORT UTF8!
				item = TF.replace_illegals(item, 'iso8859_1')
				self._pdf_prop(pw, rule.prop, item)
			else:
				# EEN CHILD-OBJECT:
				if rule.type in ['Persoon', 'Familie', 'Bron']:
					return None
				else:
					if index == 1:
						pw.font_times(12, bold=True, underlined=False, italic=True)
						pw.text_line('%s (%i):' % (rule.prop.capitalize(), count))
					pw.font_default()
					item.naar_pdf(None, pw)
					if index < count:
						pw.text_line('-' * 25)
					else:
						pw.text_line('_' * 50)

	def _pdf_prop(self, pw, caption, value, is_url=False):
		'''
		pw: pdf-writer
		'''
		import os

		caption = str(caption).replace('_', ' ')
		value   = '-' if value == None else str(value)

		pw.font_times(12, bold=True)
		height = pw.text_height()
		pw.fixed_block(45, height, caption.capitalize())
		pw.fixed_block(1, height, ':', align='center')
		pw.font_default()
		if is_url:
			caption = os.path.basename((value))
			url		= "../Fotos/%s" % caption
			pw.link(caption, url)
			pw.new_line(height=-1)
		else:
			pw.auto_block(140, height, value, newline=True)


#endregion
