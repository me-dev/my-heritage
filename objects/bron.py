from utils.metadata 	import Metadata
from objects.entiteit	import Entiteit

class Bron(Entiteit):
	'''
	De wrapper-class voor de records die betrekking hebben op externe bronnen (SOUR).
	Bijzonder van deze entiteit is dat hij op het hoogste niveau (0) voorkomt en op lagere.
	De inhoud lijkt dan niet helemaal hetzelfde te zijn, maar we gebruiken voorlopig deze ene class voor beide.
	'''

	def __init__(self, app, record0):
		classname = self.__class__.__name__
		if not classname in app.metadata:
			app.metadata[classname] = Metadata([
				# PROPERTY      		NUM	TYPE			CONTEXT
				('_regelnummer',		1,	'int',			''),				# VERPLICHT
				('_recordnummer',		1,	'int',			''),				# VERPLICHT
				('id',					1,	'str',			'SOUR'),			# VERPLICHT (id OF_id)
				('_uuid',				1,	'str',			'SOUR/_UID'),
				('_rin',				1,	'str',			'SOUR/RIN'),
				('_data',				1,	'str',			'SOUR/DATA'),
				('_rtlsave',			1,	'str',			'SOUR/_RTLSAVE'),
				('naam',				1,	'str',			'SOUR/NAME'),
				('versie',				1,	'str',			'SOUR/VERS'),
				('organisatie',			1,	'str',			'SOUR/CORP'),
				('auteur',				1,	'str',			'SOUR/AUTH'),
				('titel',				1,	'str',			'SOUR/TITL'),
				('medium',				1,	'str',			'SOUR/_MEDI'),
				('type',				1,	'str',			'SOUR/_TYPE'),
				('uitgever',			1,	'str',			'SOUR/PUBL'),
				('gebeurtenis',			1,	'Gebeurtenis',	'SOUR/EVEN'),
				('kwaliteit',			1,	'str',			'SOUR/QUAY'),
				('bladzijde',		  999,	'str',			'SOUR/PAGE'),
				('tekst',				1,	'html',			'SOUR/TEXT'),
				('tekst',				1,	'html',			'SOUR/DATA/TEXT'),
				('tekst',				1,	'html+',		'SOUR/TEXT/CONC'),		# html+ BETEKENT CONCATENATIE; NUM ALS 1E PROP.
				('tekst',				1,	'html+',		'SOUR/TEXT/CONT'),
				('tekst',				1,	'html+',		'SOUR/DATA/TEXT/CONC'),
				('tekstdatum',			1,	'str',			'SOUR/DATA/DATE'),
			])
		super().__init__(app, record0, app.metadata[classname])
		self.verwerk(record0)

	def kop(self):
		'''Overschrijft die van de base-class met iets uitgebreiders.'''
		if self.titel:
			return "%s: %s" % (self._classname, self.titel)
		elif self.id:
			return "%s: %s" % (self._classname, self.id)
		else:
			return super().kop()

	def naar_html(self, template=None, fragment=False):
		'''Produceert de HTML die door de eigen en andere pagina's gebruikt kan worden.'''
		html = super().naar_html(template=None, fragment=True)
		if template:
			template = template.replace('###NAAM###', self.titel)
			return template.replace('###EIGENSCHAPPEN###', html)
		else:
			return html
