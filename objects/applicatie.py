import utils.filesystem_functions	as FS
import utils.print_functions		as PF
import utils.text_functions			as TF
import utils.htmlbuilder            as HB
from objects.record					import Record
from objects.metadata 				import Metadata
from objects.persoon				import Persoon
from objects.familie				import Familie
from objects.bron 					import Bron
from objects.album 					import Album
from objects.bestand				import Bestand

class Applicatie:
	'''De wrapper-class die alles aanstuurt.'''

	def __init__(self, config_file):
		self.config_file	= config_file
		self.records		= None
		self.entiteiten		= None
		self.dic_ids		= {}		# VOOR SNELLER ZOEKEN
		self.metadata		= {}

	def __repr__(self):
		return 'Applicatie (MyHeritage-script)'

	def __str__(self):
		return 'Applicatie (MyHeritage-script)'

	def lees_configuratie(self):
		self.config			= FS.read_cfg(path=self.config_file, keys=['modus', 'data', 'bestanden', 'gedcom', 'html_start'])
		modus				= int(self.config['modus'])
		if modus < 0:
			modus = 0
		if modus > 3:
			modus = 3
		self.modus			= modus
		self.verbose		= (modus == 1 or modus == 3)
		self.gedcombestand	= "%s/%s" % (self.config['data'], self.config['gedcom'])
		self.plaatjesdir	= "%s/%s" % (self.config['data'], self.config['bestanden'])
		self.export_tekst	= "%s/txt" % self.config['data']
		self.export_json	= "%s/json" % self.config['data']
		self.export_pdf		= "%s/pdf" % self.config['data']
		self.export_xml		= "%s/xml" % self.config['data']
		self.export_html	= "%s/html" % self.config['data']
		self.html_index		= "%s/html/index.html" % self.config['data']
		if self.config['html_start']:
			self.html_start	= "%s/html/%s" % (self.config['data'], self.config['html_start'])
		else:
			self.html_start	= None	# WAT BETEKENT DAT INDEX.HTML GEBRUIKT ZAL WORDEN.

	def check_configuratie(self):
		TEMPLATE = "%-40s: %s"

		dic_modus = {0: "Gebruiker", 1: "Gebruiker verbose", 2: "Ontwikkelaar", 3: "Ontwikkelaar verbose"}
		if self.modus < 2:
			PF.green(TEMPLATE % ("Modus", "%i (%s)" % (self.modus, dic_modus[self.modus])))
		else:
			PF.yellow(TEMPLATE % ("Modus", "%i (%s)" % (self.modus, dic_modus[self.modus])))

		if self.verbose:
			PF.yellow(TEMPLATE % ("Verbose", self.verbose))
		else:
			PF.green(TEMPLATE % ("Verbose", self.verbose))

		if FS.exists_file(self.gedcombestand):
			datum = FS.datetime_modified(self.gedcombestand)
			PF.green(TEMPLATE % ("Tekstbestand (.ged) bestaat", "%s (van %s)" % (self.gedcombestand, datum)))
			self.check_regeleindes()
			if self.separator_warning:
				PF.red(TEMPLATE % ('Regeleindes', self.separator_warning))
				PF.red(TEMPLATE % ("Scheiding voor records", "%s (%i)" % ('CRLF' if self.record_separator == '\r\n' else 'LF', self.record_count)))
				PF.red(TEMPLATE % ("Scheiding voor regels",  "%s (%i)" % ('CRLF' if self.line_separator   == '\r\n' else 'LF', self.line_count)))
			else:
				PF.green(TEMPLATE % ("Scheiding voor records", "%s (%i)" % ('CRLF' if self.record_separator == '\r\n' else 'LF', self.record_count)))
				PF.green(TEMPLATE % ("Scheiding voor regels",  "%s (%i)" % ('CRLF' if self.line_separator   == '\r\n' else 'LF', self.line_count)))

		else:
			PF.red(TEMPLATE % ("Tekstbestand (.ged) bestaat niet", self.gedcombestand))

		if FS.exists_directory(self.plaatjesdir):
			PF.green(TEMPLATE % ("Directory met foto's bestaat", self.plaatjesdir))
		else:
			PF.red(TEMPLATE % ("Directory met foto's bestaat niet", self.plaatjesdir))

		if FS.exists_directory(self.export_html):
			PF.green(TEMPLATE % ("Directory HTML-export bestaat", self.export_html))
		else:
			PF.yellow(TEMPLATE % ("Directory HTML-export bestaat nog niet", self.export_html))

		if FS.exists_file(self.html_index):
			PF.green(TEMPLATE % ("HTML-index bestaat", self.html_index))
		else:
			PF.yellow(TEMPLATE % ("HTML-index bestaat nog niet", self.html_index))

		if self.html_start:
			if FS.exists_file(self.html_start):
				PF.green(TEMPLATE % ("HTML-start bestaat", self.html_start))
			else:
				PF.yellow(TEMPLATE % ("HTML-start bestaat nog niet", self.html_start))
		else:
			PF.yellow(TEMPLATE % ("HTML-start bestaat niet", "I.p.v. hiervan zal index.html gebruikt worden."))

		if FS.exists_directory(self.export_tekst):
			PF.green(TEMPLATE % ("Directory tekst-export bestaat", self.export_tekst))
		else:
			PF.yellow(TEMPLATE % ("Directory tekst-export bestaat nog niet", self.export_tekst))

		if FS.exists_directory(self.export_json):
			PF.green(TEMPLATE % ("Directory JSON-export bestaat", self.export_json))
		else:
			PF.yellow(TEMPLATE % ("Directory JSON-export bestaat nog niet", self.export_json))

		if FS.exists_directory(self.export_xml):
			PF.green(TEMPLATE % ("Directory XML-export bestaat", self.export_xml))
		else:
			PF.yellow(TEMPLATE % ("Directory XML-export bestaat nog niet", self.export_xml))

		if FS.exists_directory(self.export_pdf):
			PF.green(TEMPLATE % ("Directory PDF-export bestaat", self.export_pdf))
		else:
			PF.yellow(TEMPLATE % ("Directory PDF-export bestaat nog niet", self.export_pdf))

		installed_fpdf = False
		try:
			bla = __import__('fpdf')
			installed_fpdf = True
		except:
			pass
		if installed_fpdf:
			PF.green(TEMPLATE % ("Python-module is herkend", "fpdf, voor het aanmaken van PDFs."))
		else:
			PF.red(TEMPLATE % ("Python-module is niet herkend", "fpdf, voor het aanmaken van PDFs: $ pip install fpdf"))

	def check_regeleindes(self):
		'''
		Onderzoekt hoe in de gedcom-file regeleindes gebruikt zijn, en hangt deze dan aan het applicatie-object.
		Vult .separator_warning met None of een waarschuwing, en moet daarom eerst via check-configuratie gecontroleerd worden.
		'''
		text	= FS.read_utf8(self.gedcombestand)
		crlfs	= text.count('\r\n')		# Bijv.  99 of   1 of   0
		lfs 	= text.count('\n') - crlfs	# Bijv. 100 of 100 of 100 -> 1 of 99 of 100
		if lfs > crlfs and crlfs > 0:
			self.record_separator 	= '\n'
			self.record_count		= lfs
			self.line_separator		= '\r\n'
			self.line_count			= crlfs
			self.separator_warning	= None
			return None
		elif crlfs > lfs and lfs > 0:
			self.record_separator 	= '\r\n'
			self.record_count		= crlfs
			self.line_separator		= '\n'
			self.line_count			= lfs
			self.separator_warning	= None
		elif lfs == 0:
			self.record_separator 	= '\r\n'
			self.record_count		= crlfs
			self.line_separator		= '\n'
			self.line_count			= lfs
			self.separator_warning	= 'CRLF wordt gebruikt om records te scheiden; er is geen LF gevonden waarmee meerregelige teksten gescheiden worden.'
		elif crlfs == 0:
			self.record_separator 	= '\n'
			self.record_count		= lfs
			self.line_separator		= '\r\n'
			self.line_count			= crlfs
			self.separator_warning	= 'LF wordt gebruikt om records te scheiden; er is geen CRLF gevonden waarmee meerregelige teksten gescheiden worden.'

	def repareer_gedcom(self):
		'''
		Repareert foutieve coderingen door MyHeritage.
		Met name è en ë aan het einde van regels worden verkeerd afgebroken en geven 2 corrupte regels.
		'''
		raw_lines	= FS.read_lines_raw(self.gedcombestand, newline=b'\r\n', strip=True)

		line_count	= 0
		err_count	= 0
		for raw_line in raw_lines:
			line_count += 1
			try:
				line = raw_line.decode('utf-8')
			except Exception as x:
				err_count += 1
				print(f"Fout op regel {line_count}: {str(x)}")
				print("   ", raw_line)
				print()
		print(f"Aantal fouten: {err_count}")

		# EXIT ALS ER GEEN FOUTEN ZIJN:
		if err_count == 0:
			print("Geen reparaties noodzakelijk.")
			return

		# MAAK EEN BACKUP, ALS DAT NIET EERDER GEBEURD IS:
		backup = self.gedcombestand.replace(".ged", "") + ".backup"		# OM ZEKER TE ZIJN VAN EEN NIEUWE NAAM
		if not FS.exists_file(backup):
			FS.copy_file(self.gedcombestand, backup)
			FS.delete_file(self.gedcombestand)

		# PROBEER TE REPAPEREN:
		line_count		= 0
		repair_count	= 0
		fixed_lines		= []
		for raw_line in raw_lines:
			line_count += 1
			try:
				line = raw_line.decode('utf-8')
				fixed_lines.append(raw_line)
			except Exception as x:
				print(f"Fout op regel {line_count}: {str(x)}")
				print("   ", raw_line)
				fixed_line = raw_line
				fixed_line = fixed_line.replace(b'\xc3', b'')			# LAATSTE BYTE VOOR DE NEWLINE, DIE BIJ DE VOLGENDE REGEL HOORT.
				fixed_line = fixed_line.replace(b'\xa8', b'\xc3\xa8')	# COMPLETEERT è IN EEN VOLGENDE REGEL.
				fixed_line = fixed_line.replace(b'\xab', b'\xc3\xab')	# COMPLETEERT ë IN EEN VOLGENDE REGEL.
				repair_count += 1
				print(f"Gerepareerd:")
				print("   ", fixed_line)
				print()
		print(f"Aantal correcties: {repair_count}")

		# MAAK EEN NIEUW BESTAND:
		data = b'\r\n'.join(fixed_lines)
		FS.write_bytes(data, self.gedcombestand)
		print("Een nieuw bestand is aangemaakt; run opnieuw om dit te testen!")

	def dump_regels(self, level):
		'''
		-1 = Checken op niet begrepen regels.
		 0 = Alleen regels van level 0.
		 1 = Alleen regels t/m level 1. enz.
		'''

		self.check_regeleindes()
		if self.separator_warning:
			PF.red(self.separator_warning)
			exit()

		lines = FS.read_records_utf8(self.gedcombestand, self.record_separator)
		num   = 0
		for line in lines:
			num += 1
			pos = line.find(' ')
			if pos == -1 and level == -1:
				PF.red("Line %i: %s" % (num, line))
			else:
				try:
					line_level = int(line[:pos])
					if line_level <= level:
						print(line)
				except:
					PF.red("Line %i: %s" % (num, line))
			if line.startswith('0 TRLR'):	# TRAILER = EIND
				break

	def lees_records(self):
		'''Vult eenmalig self.records met data.'''
		self.check_regeleindes()
		if self.separator_warning:
			PF.red(self.separator_warning)
			exit()

		if not self.records:
			self.records 	= []
			regelnummer 	= 1
			recordnummer	= 1
			lines 			= FS.read_records_utf8(self.gedcombestand, self.record_separator)

			for line in lines:
				record = Record(regelnummer, recordnummer, line)
				recordnummer += 1
				regelnummer  += record.regelaantal
				self.records.append(record)
				if line.startswith('0 TRLR'):	# TRAILER = EIND
					break;
		return self.records

	def print_records(self, max=1000000, filter=None, exact=False, eerste_regel=0, laatste_regel=1000000, eerste_record=0, laatste_record=1000000):
		'''
		Print de letterlijke data uit het tekstbestand.
		max   : Het max. te printen aantal regels; met name bij initiele ontwikkeling handig.
		filter: Een optionele zoekterm.
		exact : Hoofdlettergevoelig  of niet (= standaard).
		'''

		count	= 0
		records = self.lees_records()
		for record in records:
			if record.regelnummer < eerste_regel or record.regelnummer > laatste_regel:
				continue
			if record.recordnummer < eerste_record or record.recordnummer > laatste_record:
				continue
			if not filter:
				matching = True
			elif exact:
				matching = (filter in str(record.data))
			else:
				matching = (filter.lower() in str(record.data).lower())
			if matching:
				print(record)
				count += 1
				if count >= max:
					break

	def maak_entiteiten(self, max=1000000):
		'''
		Vult eenmalig self.entitetien met geinterpreteerde records.
		max: Met name tijdens het ontwikkelen handig.
		'''
		if not self.entiteiten:
			self.entiteiten = []
			entiteit		= None
			count 			= 0
			records 		= self.lees_records()
			for record in records:
				count += 1
				if count > max:
					break

				if record.level == 0:
					# VOORGAANDE AFSLUITEN?
					if entiteit:
						self.entiteiten.append(entiteit)

					# ONDERSTEUNDE ENTITEITEN:
					if record.label == 'HEAD':
						entiteit = Metadata(self, record)
					elif record.label == '':
						# REGEL MET EXTRA SPATIE VOOR '_PUBLISH':
						entiteit = None
					elif record.label == 'SUBM':
						entiteit = None
					elif record.label == 'INDI':
						entiteit = Persoon(self, record)
						self.dic_ids[entiteit.id] = entiteit
					elif record.label == 'FAM':
						entiteit = Familie(self, record)
						self.dic_ids[entiteit.id] = entiteit
					elif record.label == 'SOUR':
						entiteit = Bron(self, record)
						self.dic_ids[entiteit.id] = entiteit
					elif record.label == 'ALBUM':
						entiteit = Album(self, record)
						self.dic_ids[entiteit.id] = entiteit
					elif record.label == 'TRLR':
						# TRAILER, MARKEERT HET EIND.
						entiteit = None
					else:
						if self.modus >= 2:
							raise Exception("Entiteit '%s': regel %i, record %i' wordt nog niet ondersteund!" % (record.label, record.regelnummer, record.recordnummer))
						else:
							# VOOR NIET-ONDERSTEUNDE MOETEN WE VOLGENDE RECORDS NEGEREN:
							entiteit = None
				else:
					# VERVOLG-RECORDS MET LEVEL > 0:
					if entiteit:
						entiteit.verwerk(record)

			# LAATSTE ENTITEIT?
			# (IN DE PRAKTIJK ZAL MET 0 TRLR AAN HET EIND NETJE AFGESLOTEN WORDEN,
			#  MAAR MET MAX. KAN ER NOG EENTJE HALF IN VERWERKING ZIJN!)
			if entiteit:
				self.entiteiten.append(entiteit)

			# ENTITEITEN BEVATTEN NOG VEEL ID'S, WAARVOOR WE ECHTE OBJECT-VERWIJZINGEN WILLEN HEBBEN:
			# (DEZE ENTITEITEN MOETEN HIERBIJ ZONODIG ZELF HUN SUB-ENTITEITEN VULLEN!)
			for entiteit in self.entiteiten:
				if isinstance(entiteit, Persoon):
					entiteit.vul_verwijzingen(self.dic_ids)
				if isinstance(entiteit, Familie):
					entiteit.vul_verwijzingen(self.dic_ids)
		return self.entiteiten

	def geef_alle(self):
		self.maak_entiteiten()
		return self.entiteiten

	def geef_personen(self):
		self.maak_entiteiten()
		entiteiten = [e for e in self.entiteiten if isinstance(e, Persoon)]
		entiteiten = sorted(entiteiten, key=lambda persoon: str(persoon.achternaam) + ', ' + str(persoon.voornamen))
		return entiteiten

	def geef_families(self):
		self.maak_entiteiten()
		entiteiten = [e for e in self.entiteiten if isinstance(e, Familie)]
		entiteiten = sorted(entiteiten, key=lambda familie: str(familie.naam_kort))
		return entiteiten

	def geef_bronnen(self):
		self.maak_entiteiten()
		entiteiten = [e for e in self.entiteiten if isinstance(e, Bron)]
		entiteiten = sorted(entiteiten, key=lambda bron: str(bron.titel))
		return entiteiten

	def geef_overige(self):
		self.maak_entiteiten()
		return [e for e in self.entiteiten if isinstance(e, Metadata) or  isinstance(e, Album)]

	def zoek_personen(self, woord):
		self.maak_entiteiten()
		return [e for e in self.entiteiten if isinstance(e, Persoon) and e.naam_bevat(woord)]

	def zoek_id(self, id):
		# PREPARATION:
		id = str(id).upper()
		if not id:
			id = '@?@'
		if not id.startswith('@'):
			id = '@' + id
		if not id.endswith('@'):
			id += '@'
		if id[1].isnumeric():
			# ZOEK STANDAARD NAAR PERSONEN (I):
			id = id[0] + 'I' + id[1:]

		self.maak_entiteiten()
		if id in self.dic_ids:
			entiteit = self.dic_ids[id]
			return [entiteit]
		else:
			return []
		# return [e for e in self.entiteiten if not isinstance(e, Metadata) and e.id == id]

	def print_regels_id(self, id):
		'''Print de letterlijke regels voor een gegeven entiteit.'''
		entiteiten = self.zoek_id(id)
		if len(entiteiten) == 0:
			PF.red("Geen entiteiten gevonden met dit ID!")
		else:
			for record in entiteiten[0]._records:
				print("Regel %i Record %i: %s" % (record.regelnummer, record.recordnummer, record.data))

	def maak_html(self):
		# VOORBEREIDING:
		KOLOMMEN 	= 4
		html		= FS.read_utf8('./templates/index.html')
		pad 		= FS.path_absolute(self.export_html)
		if FS.exists_directory(pad):
			FS.delete_directory(pad)
		FS.create_directory(pad)
		FS.copy_file('./templates/style.css', pad + '/style.css')

		# DOORLOOP PERSONEN:
		hb 				= HB.HtmlBuilder(fragment=True)
		template		= FS.read_utf8('./templates/persoon.html')
		personen 		= self.geef_personen()
		kolom   		= 0
		for persoon in personen:
			# EIGEN BESTAND:
			FS.write_utf8(persoon.naar_html(template), "%s/%s.html" % (pad, persoon.id))
			# TOEVOEGING AAN INDEX:
			if kolom == 0:
				hb.fragment.tr		= HB.Tr()
			hb.fragment.tr.td		= HB.Td()
			hb.fragment.tr.td.link 	= HB.Link("%s, %s" % (persoon.achternaam, persoon.voornamen), "%s.html" % persoon.id, target='_blank')
			kolom = (kolom + 1) % KOLOMMEN
		# SLUIT AF:
		html = html.replace('###PLAATS-PERSONEN###', hb.to_html(tab=''))

		# DOORLOOP FAMILIES:
		hb 				= HB.HtmlBuilder(fragment=True)
		template		= FS.read_utf8('./templates/familie.html')
		families 		= self.geef_families()
		kolom   		= 0
		for familie in families:
			# EIGEN BESTAND:
			FS.write_utf8(familie.naar_html(template), "%s/%s.html" % (pad, familie.id))
			# TOEVOEGING AAN INDEX:
			if kolom == 0:
				hb.fragment.tr		= HB.Tr()
			hb.fragment.tr.td		= HB.Td()
			hb.fragment.tr.td.link 	= HB.Link(familie.naam_kort, "%s.html" % familie.id, target='_blank')
			kolom = (kolom + 1) % KOLOMMEN
		# SLUIT AF:
		html = html.replace('###PLAATS-FAMILIES###', hb.to_html(tab=''))

		# DOORLOOP BRONNEN:
		hb 				= HB.HtmlBuilder(fragment=True)
		template		= FS.read_utf8('./templates/bron.html')
		bronnen 		= self.geef_bronnen()
		KOLOMMEN		= 2
		kolom   		= 0
		for bron in bronnen:
			# EIGEN BESTAND:
			FS.write_utf8(bron.naar_html(template), "%s/%s.html" % (pad, bron.id))
			# TOEVOEGING AAN INDEX:
			if kolom == 0:
				hb.fragment.tr 		= HB.Tr()
			hb.fragment.tr.td 		= HB.Td()
			hb.fragment.tr.td.link 	= HB.Link(bron.titel, "%s.html" % bron.id, target='_blank')
			kolom = (kolom + 1) % KOLOMMEN
		# SLUIT AF:
		html = html.replace('###PLAATS-BRONNEN###', hb.to_html(tab=''))

		# MAAK HET INDEX-BESTAND:
		FS.write_utf8(html, pad + '/index.html')

	def html_id(self, id):
		# PREPARATION:
		id = str(id).upper()
		if not id:
			id = '@?@'
		if not id.startswith('@'):
			id = '@' + id
		if not id.endswith('@'):
			id += '@'
		if id[1].isnumeric():
			# ZOEK STANDAARD NAAR PERSONEN (I):
			id = id[0] + 'I' + id[1:]

		pad = "%s/%s/%s.html" % (self.config['data'], self.config['export_html'], id)
		if not FS.exists_file(pad):
			return None
		else:
			return pad

	def maak_tekstbestanden(self):
		'''Maakt zo eenvoudig mogelijke tekstbestanden per entiteit, voor eventuele import door andere applicaties later.'''

		index	= TF.StandardStringBuilder()
		pad		= FS.path_absolute(self.export_tekst)
		if FS.exists_directory(pad):
			FS.delete_directory(pad)
		FS.create_directory(pad)

		# DOORLOOP PERSONEN:
		personen = self.geef_personen()
		index.addline('INDEX PERSONEN (%i):' % len(personen), count=2)
		for persoon in personen:
			FS.write_utf8(persoon.naar_tekst(), "%s/%s.txt" % (pad, persoon.id))
			index.addline('%-20s%s' % (persoon.id, persoon.kop()))
		index.addline()

		# DOORLOOP FAMILIES:
		families = self.geef_families()
		index.addline('INDEX FAMILIES (%i):' % len(families), count=2)
		for familie in families:
			FS.write_utf8(familie.naar_tekst(), "%s/%s.txt" % (pad, familie.id))
			index.addline('%-20s%s' % (familie.id, familie.kop()))
		index.addline()

		# DOORLOOP BRONNEN:
		bronnen = self.geef_bronnen()
		index.addline('INDEX BRONNEN (%i):' % len(bronnen), count=2)
		for bron in bronnen:
			FS.write_utf8(bron.naar_tekst(), "%s/%s.txt" % (pad, bron.id))
			index.addline('%-20s%s' % (bron.id, bron.kop()))

		# FINISH INDEX:
		FS.write_utf8(index.text(), "%s/Alle-Index.txt" % pad)
		index.close()

	def maak_json(self):
		'''
		Maakt zo eenvoudig mogelijke JSON-bestanden per entiteit, voor eventuele import door andere applicaties later.
		(JSON = JavaScript Object Notation)
		'''

		index	= TF.StandardStringBuilder()
		pad		= FS.path_absolute(self.export_json)
		if FS.exists_directory(pad):
			FS.delete_directory(pad)
		FS.create_directory(pad)

		# DOORLOOP PERSONEN:
		personen = self.geef_personen()
		index.addline('INDEX PERSONEN (%i):' % len(personen), count=2)
		for persoon in personen:
			FS.write_utf8(persoon.naar_json(), "%s/%s.json" % (pad, persoon.id))
			index.addline('%-20s%s' % (persoon.id, persoon.kop()))
		index.addline()

		# DOORLOOP FAMILIES:
		families = self.geef_families()
		index.addline('INDEX FAMILIES (%i):' % len(families), count=2)
		for familie in families:
			FS.write_utf8(familie.naar_json(), "%s/%s.json" % (pad, familie.id))
			index.addline('%-20s%s' % (familie.id, familie.kop()))
		index.addline()

		# DOORLOOP BRONNEN:
		bronnen = self.geef_bronnen()
		index.addline('INDEX BRONNEN (%i):' % len(bronnen), count=2)
		for bron in bronnen:
			FS.write_utf8(bron.naar_json(), "%s/%s.json" % (pad, bron.id))
			index.addline('%-20s%s' % (bron.id, bron.kop()))

		# FINISH INDEX:
		FS.write_utf8(index.text(), "%s/Alle-Index.txt" % pad)
		index.close()

	def maak_xml(self):
		'''
		Maakt zo eenvoudig mogelijke XML-bestanden per entiteit, voor eventuele import door andere applicaties later.
		'''

		index	= TF.StandardStringBuilder()
		pad		= FS.path_absolute(self.export_xml)
		if FS.exists_directory(pad):
			FS.delete_directory(pad)
		FS.create_directory(pad)

		# DOORLOOP PERSONEN:
		personen = self.geef_personen()
		index.addline('INDEX PERSONEN (%i):' % len(personen), count=2)
		for persoon in personen:
			FS.write_utf8(persoon.naar_xml(), "%s/%s.xml" % (pad, persoon.id))
			index.addline('%-20s%s' % (persoon.id, persoon.kop()))
		index.addline()

		# DOORLOOP FAMILIES:
		families = self.geef_families()
		index.addline('INDEX FAMILIES (%i):' % len(families), count=2)
		for familie in families:
			FS.write_utf8(familie.naar_xml(), "%s/%s.xml" % (pad, familie.id))
			index.addline('%-20s%s' % (familie.id, familie.kop()))
		index.addline()

		# DOORLOOP BRONNEN:
		bronnen = self.geef_bronnen()
		index.addline('INDEX BRONNEN (%i):' % len(bronnen), count=2)
		for bron in bronnen:
			FS.write_utf8(bron.naar_xml(), "%s/%s.xml" % (pad, bron.id))
			index.addline('%-20s%s' % (bron.id, bron.kop()))

		# FINISH INDEX:
		FS.write_utf8(index.text(), "%s/Alle-Index.txt" % pad)
		index.close()

	def maak_pdf(self):
		'''
		Maakt PDF-bestanden per (hoofd-)entiteit.
		'''

		index	= TF.StandardStringBuilder()
		pad		= FS.path_absolute(self.export_pdf)
		if FS.exists_directory(pad):
			FS.delete_directory(pad)
		FS.create_directory(pad)

		# DOORLOOP PERSONEN:
		personen = self.geef_personen()
		index.addline('INDEX PERSONEN (%i):' % len(personen), count=2)
		for persoon in personen:
			#if not persoon.id in ["@I500001@", "@I500003@", "@I500007@"]:	# (OM MAAR EEN PAAR TE TESTEN)
			#	continue
			persoon.naar_pdf("%s/%s.pdf" % (pad, persoon.id))
			index.addline('%-20s%s' % (persoon.id, persoon.kop()))
		index.addline()
		#return

		# DOORLOOP FAMILIES:
		families = self.geef_families()
		index.addline('INDEX FAMILIES (%i):' % len(families), count=2)
		for familie in families:
			familie.naar_pdf("%s/%s.pdf" % (pad, familie.id))
			index.addline('%-20s%s' % (familie.id, familie.kop()))
		index.addline()

		# DOORLOOP BRONNEN:
		bronnen = self.geef_bronnen()
		index.addline('INDEX BRONNEN (%i):' % len(bronnen), count=2)
		for bron in bronnen:
			bron.naar_pdf("%s/%s.pdf" % (pad, bron.id))
			index.addline('%-20s%s' % (bron.id, bron.kop()))

		# FINISH INDEX:
		FS.write_utf8(index.text(), "%s/Alle-Index.txt" % pad)
		index.close()

		# FS.file_open("%s/@I500003@.pdf" % pad)

	def maak_formulier(self, id):
		'''
		Maakt een tekstbestand met wat bekend is, ter completering door andere mensen.
		Het is aan de caller om iets met deze tekst te doen, bijv. opslaan als tekstbestand.
		Voorlopig alleen voor familie-ids, die dan ook verondersteld worden ("1" -> "@F1@").
		De returnvalue is None of het absolute pad naar het nieuwe bestand.
		Dit tekstbestand wordt voor het gemak van de ontvanger opgemaakt in Microsoft-formaat (Unicode-LE met CRLF).
		'''

		# PREPARATION:
		id = str(id).upper()
		if not id:
			id = '@?@'
		if not id.startswith('@'):
			id = '@' + id
		if not id.endswith('@'):
			id += '@'
		if id[1].isnumeric():
			# ZOEK STANDAARD NAAR FAMILIES (I):
			id = id[0] + 'F' + id[1:]

		self.maak_entiteiten()
		if id in self.dic_ids:
			familie = self.dic_ids[id]
		if not familie or not isinstance(familie, Familie):
			return None

		stamp 	= TF.timestamp(size=12)
		pad		= "%s/Formulier_Familie_%s-%s-%s.txt" % (self.export_tekst, id, stamp[0:8], stamp[8:12])
		tekst	= familie.maak_formulier().replace('\n', '\r\n')
		FS.write_unicode(tekst, pad)
		return pad

	#region ----- TEST-FUNCTIES -----

	def test(self):
		'''Functie voor ad-hoc tests tijdens het ontwikkelen.'''
		self._test_environment()

	def _test_environment(self):
		print('Globals:')
		i = 0
		for key in globals():
			i += 1
			print("   %i) %s: %s" % (i, key, globals()[key]))
			# GEEFT BIJV.:   15) Persoon: <class 'objects.persoon.Persoon'>
		print()

		i = 0
		for key in locals():
			i += 1
			print("   %i) %s: %s" % (i, key, locals()[key]))
		print()

		i = 0
		for key in vars():
			i += 1
			print("   %i) %s: %s" % (i, key, vars()[key]))
		print()

	#endregion

	__fix_region_bug_vscode = "VSCode wil de laatste ingesprongen region niet dichtklappen, tenzij er iets op volgt!"
