from utils.metadata 		import Metadata
from objects.entiteit		import Entiteit
import utils.textbuilder	as TB
import utils.htmlbuilder	as HB

class Familie(Entiteit):
	'''
	De wrapper-class voor de records die betrekking hebben op opleidingen (FAM).
	'''

	def __init__(self, app, record0):
		classname = self.__class__.__name__
		if not classname in app.metadata:
			app.metadata[classname] = Metadata([
				# PROPERTY      		NUM	TYPE			CONTEXT
				('_regelnummer',		1,	'int',			''),				# VERPLICHT
				('_recordnummer',		1,	'int',			''),				# VERPLICHT
				('id',					1,	'str',			'FAM'),				# VERPLICHT (id OF_id)
				('naam_kort',			1,	'str',			''),
				('naam_lang',			1,	'str',			''),
				('_uuid',				1,	'str',			'FAM/_UID'),
				('_rin',				1,	'str',			'FAM/RIN'),
				('_enga',				1,	'str',			'FAM/ENGA'),		# VERLOVING; NOG NIET GEVULD GEZIEN.
				('_enga_uuid',			1,	'str',			'FAM/ENGA/_UID'),
				('_enga_rin',			1,	'str',			'FAM/ENGA/RIN'),
				('_marb',				1,	'str',			'FAM/MARB'),		# ONDERTROUW (An event of an official public notice given that two people intend to marry.)
				('_marb_uuid',			1,	'str',			'FAM/MARB/_UID'),
				('_marb_rin',			1,	'str',			'FAM/MARB/RIN'),
				('_marl',				1,	'str',			'FAM/MARL'),		# ONDERTROUW (An event of obtaining a legal license to marry.)
				('_marl_uuid',			1,	'str',			'FAM/MARL/_UID'),
				('_marl_rin',			1,	'str',			'FAM/MARL/RIN'),
				('_marr',				1,	'str',			'FAM/MARR'),		# HUWELIJK
				('_marr_uuid',			1,	'str',			'FAM/MARR/_UID'),
				('_marr_rin',			1,	'str',			'FAM/MARR/RIN'),
				('_marr_note_desc',		1,	'str',			'FAM/MARR/NOTE/_DESCRIPTION'),
				('_marr_type',			1,	'str',			'FAM/MARR/TYPE'),	# LIJKT NIETS ANDERS TE BEVATTEN DAN 'MARR'!?
				('_div',				1,	'str',			'FAM/DIV'),			# ECHTSCHEIDING
				('_div_uuid',			1,	'str',			'FAM/DIV/_UID'),
				('_div_rin',			1,	'str',			'FAM/DIV/RIN'),
				('_div_note_desc',		1,	'str',			'FAM/DIV/NOTE/_DESCRIPTION'),
				('man_id',				1,	'str',			'FAM/HUSB'),
				('man',					1,	'Persoon',		''),
				('vrouw_id',			1,	'str',			'FAM/WIFE'),
				('vrouw',				1,	'Persoon',		''),
				('kind_ids',		  999,	'str',			'FAM/CHIL'),
				('kinderen',		  999,	'Persoon',		''),
				('datum_ondertrouw',	1,	'str',			'FAM/MARB/DATE'),
				('plaats_ondertrouw',	1,	'str',			'FAM/MARB/PLAC'),
				('datum_ondertrouw',	1,	'str',			'FAM/MARL/DATE'),
				('plaats_ondertrouw',	1,	'str',			'FAM/MARL/PLAC'),
				('datum_huwelijk',		1,	'str',			'FAM/MARR/DATE'),
				('plaats_huwelijk',		1,	'str',			'FAM/MARR/PLAC'),
				('opmerking_huwelijk',	1,	'str',			'FAM/MARR/NOTE'),
				('bestanden_huwelijk',999,	'Bestand',		'FAM/MARR/OBJE'),
				('datum_scheiding',		1,	'str',			'FAM/DIV/DATE'),
				('plaats_scheiding',	1,	'str',			'FAM/DIV/PLAC'),
				('opmerking_scheiding',	1,	'str',			'FAM/DIV/NOTE'),
				('bestanden',		  999,	'Bestand',		'FAM/OBJE'),
				('gebeurtenissen',	  999,	'Gebeurtenis',	'FAM/EVEN'),
			])
		super().__init__(app, record0, app.metadata[classname])
		self.verwerk(record0)

	def vul_verwijzingen(self, dic_ids):
		'''Wordt door de applicatie expliciet aangeroepen om met de ID's de juiste entiteiten te vinden.'''
		if self.man_id and self.man_id in dic_ids:
			self.man = dic_ids[self.man_id]
		if self.vrouw_id and self.vrouw_id in dic_ids:
			self.vrouw = dic_ids[self.vrouw_id]
		for kind_id in self.kind_ids:
			if kind_id and kind_id in dic_ids:
				self.kinderen.append(dic_ids[kind_id])
			else:
				self.kinderen.append(None)
		for bestand in self.bestanden:
			bestand.vul_verwijzingen(dic_ids)
		for bestand in self.bestanden_huwelijk:
			bestand.vul_verwijzingen(dic_ids)

		# FABRICEER NAMEN M.B.V. MAN EN VROUW:
		if self.man and self.man.achternaam:
			self.naam_kort = self.man.achternaam
			self.naam_lang = "%s (%s)" % (self.man.achternaam, self.man_id)
		else:
			self.naam_kort = str(self.man_id)				# (ER BESTAAT EEN FAMILIE ZONDER MAN!)
			self.naam_lang = str(self.man_id)
		if self.vrouw and self.vrouw.achternaam:
			self.naam_kort += '/' + self.vrouw.achternaam
			self.naam_lang += " + %s (%s)" % (self.vrouw.achternaam, self.vrouw_id)
		else:
			self.naam_kort += '/' + str(self.vrouw_id)		# (EN EEN ZONDER VROUW!)
			self.naam_lang += " + " + str(self.vrouw_id)
		self.naam_lang += "[ %s]" % self.id

	def kop(self):
		'''Overschrijft die van de base-class met iets uitgebreiders.'''
		return "%s: %s" % (self._classname, self.naam_kort)

	def naar_tekst_rule(self, rule, tb, tabs, done):
		'''Overschrijft die van de base-class, om child-objecten voor personen afwijkend te behandelen.'''
		if rule.prop == 'man':
			if self.man:
				tb.prop(tabs+1, 'Man', self.man.naam)
		elif rule.prop == 'vrouw':
			if self.vrouw:
				tb.prop(tabs+1, 'Vrouw', self.vrouw.naam)
		elif rule.prop == 'kind_ids':
			pass
		elif rule.prop == 'kinderen':
			for i in range(len(self.kind_ids)):
				if self.kinderen[i]:
					value = "%s (%s)" % (self.kinderen[i].naam, self.kind_ids[i])
				else:
					value = self.kind_ids[i]
				tb.prop(tabs+1, "Kind", value)
		else:
			super().naar_tekst_rule(rule, tb, tabs, done)
		done.add(rule.prop)

	def naar_html(self, template=None, fragment=False):
		'''Produceert de HTML-rows voor de zelfstandige familie-pagina.'''
		html = template
		html  =	html.replace('###NAAM###', str(self.naam_kort))

		# LINKS EIGENSCHAPPEN:
		html = html.replace('###EIGENSCHAPPEN###', super().naar_html(template=None, fragment=True))

		# RECHTS PERSONEN:
		hb = HB.HtmlBuilder(fragment=True)
		hb.fragment.add_tr_headers('Personen', '')
		if self.man:
			hb.fragment.add_tr_proplink('Man',		self.man.naam, "%s.html" % self.man.id, target='_blank')
		else:
			hb.fragment.add_tr_proplink('Man',		self.man_id, "%s.html" % self.man_id, target='_blank')
		if self.vrouw:
			hb.fragment.add_tr_proplink('Vrouw',	self.vrouw.naam, "%s.html" % self.vrouw.id, target='_blank')
		else:
			hb.fragment.add_tr_proplink('Vrouw',	self.vrouw_id, "%s.html" % self.vrouw_id, target='_blank')
		if self.kinderen:
			for kind in self.kinderen:
				hb.fragment.add_tr_proplink('Kind',	kind.naam, "%s.html" % kind.id, target='_blank')
		else:
			for kind_id in self.kind_ids:
				hb.fragment.add_tr_proplink('Kind',	kind_id, "%s.html" % kind_id, target='_blank')
		html = html.replace('###PERSONEN###', hb.to_html(tab=''))
		return html

	def naar_html_rule(self, rule, hb, done):
		'''Overschrijft die van de base-class om child-objecten voor personen over te slaan; deze krijgen een eigen behandeling.'''
		if rule.prop in ['man_id', 'man', 'vrouw_id', 'vrouw', 'kind_ids', 'kinderen']:
			pass
		else:
			super().naar_html_rule(rule, hb, done)

	def naar_html_persoon(self, eigen=False):
		'''Produceert de HTML-rows voor de persoon-pagina (alleen de belangrijkste gegevens). Deze kan 0-veel families bevatten!'''
		hb = HB.HtmlBuilder(fragment=True)
		if eigen:
			hb.fragment.add_tr_empty(1)
			hb.fragment.add_tr_headers('Eigen familie', '')
		else:
			hb.fragment.add_tr_headers('Ouderfamilie', '')
		hb.fragment.add_tr_proplink('Id', 			self.id, "%s.html" % self.id, target='_blank')
		hb.fragment.add_tr_property('Huwelijk', 	self.datum_huwelijk)
		if self.man:
			hb.fragment.add_tr_proplink('Man',		self.man.naam, "%s.html" % self.man.id, target='_blank')
		else:
			hb.fragment.add_tr_proplink('Man',		self.man_id, "%s.html" % self.man_id, target='_blank')
		if self.vrouw:
			hb.fragment.add_tr_proplink('Vrouw',	self.vrouw.naam, "%s.html" % self.vrouw.id, target='_blank')
		else:
			hb.fragment.add_tr_proplink('Vrouw',	self.vrouw_id, "%s.html" % self.vrouw_id, target='_blank')
		if self.kinderen:
			for kind in self.kinderen:
				hb.fragment.add_tr_proplink('Kind',	kind.naam, "%s.html" % kind.id, target='_blank')
		else:
			for kind_id in self.kind_ids:
				hb.fragment.add_tr_proplink('Kind',	kind_id, "%s.html" % kind_id, target='_blank')
		return hb.to_html(tab='')

	def naar_pdf_rule(self, pw, rule):
		'''Overschrijft die van de base-class, om child-objecten voor personen afwijkend te behandelen.'''
		if rule.prop == 'man':
			if self.man:
				super()._pdf_prop(pw, 'Man', self.man.naam)
		elif rule.prop == 'vrouw':
			if self.vrouw:
				super()._pdf_prop(pw, 'Vrouw', self.vrouw.naam)
		elif rule.prop == 'kind_ids':
			pass
		elif rule.prop == 'kinderen':
			for i in range(len(self.kind_ids)):
				if self.kinderen[i]:
					value = "%s (%s)" % (self.kinderen[i].naam, self.kind_ids[i])
				else:
					value = self.kind_ids[i]
				super()._pdf_prop(pw, 'Kind', value)
		else:
			super().naar_pdf_rule(pw, rule)

	def maak_formulier(self):
		'''
		Maakt een tekst (in UTF8-formaat).
		Alleen de belangrijkste gegevens moeten getoond worden.
		Het moet duidelijk en makkelijk in te vullen zijn.
		'''
		# INLEIDING:
		tb = TB.TextBuilder(tab='    ', prop_caption_width=30)
		tb.rule(70, char='=')
		tb.line(0, "Controleformulier Familie: %s (%s)" % (self.naam_kort, self.id))
		tb.rule(70, char='=')
		tb.line(0, 'Hieronder staan de belangrijkste velden voor een gezin en zijn leden, ingevuld met wat nu bekend is.')
		tb.line(0, 'Gebruik dit formulier voor correcties/aanvullingen.')
		tb.newline()

		# DETAILS:
		tb.rule(70, char='-')
		tb.line(0, 'Details gezin:')
		tb.rule(70, char='-')
		tb.prop(0, "Datum huwelijk", self.datum_huwelijk or '')
		tb.prop(0, "Plaats huwelijk", self.plaats_huwelijk or '')
		tb.prop(0, "Opmerkingen", self.opmerking_huwelijk or '')
		tb.prop(0, "Datum scheiding", self.datum_scheiding or '')
		tb.prop(0, "Plaats scheiding", self.plaats_scheiding or '')
		tb.prop(0, "Opmerkingen", self.opmerking_scheiding or '')
		tb.prop(0, "Extra informatie?", '')
		tb.newline()

		# PERSONEN:
		self._maak_formulier_persoon(tb, 'Man', self.man)
		self._maak_formulier_persoon(tb, 'Vrouw', self.vrouw)
		for kind in self.kinderen:
			self._maak_formulier_persoon(tb, 'Kind', kind)

		return tb.text

	def _maak_formulier_persoon(self, tb, rol, persoon):
		achternaam			= persoon.achternaam 				if persoon and persoon.achternaam 			else ''
		voornamen			= persoon.voornamen					if persoon and persoon.voornamen			else ''
		roepnaam			= persoon.roepnaam					if persoon and persoon.roepnaam				else ''
		titel				= persoon.titel						if persoon and persoon.titel				else ''
		toevoeging			= persoon.toevoeging				if persoon and persoon.toevoeging			else ''
		geboortedatum		= ', '.join(persoon.geboortedatum)	if persoon and persoon.geboortedatum		else ''	# ER BESTAAT IEMAND MET 2!
		geboorteplaats 		= persoon.geboorteplaats			if persoon and persoon.geboorteplaats		else ''
		datum_overlijden	= persoon.datum_overlijden			if persoon and persoon.datum_overlijden		else ''
		plaats_overlijden	= persoon.plaats_overlijden			if persoon and persoon.plaats_overlijden	else ''
		beroep				= ', '.join(persoon.beroep)			if persoon and persoon.beroep				else ''

		tb.rule(70, char='-')
		tb.line(0, rol + ':')
		tb.rule(70, char='-')
		tb.prop(0, "Achternaam", 		achternaam)
		tb.prop(0, "Voornamen", 		voornamen)
		tb.prop(0, "Roepnaam", 			roepnaam)
		tb.prop(0, "Titel", 			titel)
		tb.prop(0, "Toevoeging", 		toevoeging)
		tb.prop(0, "Geboortedatum", 	geboortedatum)
		tb.prop(0, "Geboorteplaats", 	geboorteplaats)
		tb.prop(0, "Datum overlijden", 	datum_overlijden)
		tb.prop(0, "Plaats overlijden", plaats_overlijden)
		tb.prop(0, "Beroep", 			beroep)
		tb.prop(0, "Extra informatie?",	'')
		tb.newline()
		tb.newline()
		tb.line(0, "Alle extra's zijn welkom bijv. opleidingen, immigratie, emigratie, portretfoto enz.")
		tb.newline()
