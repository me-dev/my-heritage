from utils.metadata 		import Metadata
from objects.entiteit 		import Entiteit
import utils.htmlbuilder    as HB

class Persoon(Entiteit):
	'''
	De wrapper-class voor de records die betrekking hebben op een persoon.
	Dit is de meeste uitgebreide entiteit-derivaat!
	'''

	def __init__(self, app, record0):
		classname = self.__class__.__name__
		if not classname in app.metadata:
			app.metadata[classname] = Metadata([
				# PROPERTY      				NUM	TYPE			CONTEXT
				('_regelnummer',				1,	'int',			''),				# VERPLICHT
				('_recordnummer',				1,	'int',			''),				# VERPLICHT
				('id',							1,	'str',			'INDI'),			# VERPLICHT (id OF_id)
				('_idx',						1,	'str',			'INDI/'),			# FOUTE REGEL IN GEDCOM MET ALLEEN "1 "
				('_uuid',						1,	'str',			'INDI/_UID'),
				('_uuidx',						1,	'str',			'INDI//_UID'),		# DOOR FOUTE REGEL IN GEDCOM MET ALLEEN "1 "
				('_rin',					 	1,	'str',			'INDI/RIN'),
				('_rinx',						1,	'str',			'INDI//RIN'),		# DOOR FOUTE REGEL IN GEDCOM MET ALLEEN "1 "
				('_occu_uuid',				  999,	'str',			'INDI/OCCU/_UID'),	# (OCCUPATION)
				('_occu_rin',				  999,	'str',			'INDI/OCCU/RIN'),
				('_complete',					1,	'str',			'INDI/_COMPLETE'),
				('_birth',						1,	'str',			'INDI/BIRT'),
				('_birth_uuid',				  999,	'str',			'INDI/BIRT/_UID'),
				('_birth_rin',				  999,	'str',			'INDI/BIRT/RIN'),
				('_birth_note_desc',			1,	'str',			'INDI/BIRT/NOTE/_DESCRIPTION'),
				('_death',					  999,	'str',			'INDI/DEAT'),
				('_death_uuid',				  999,	'str',			'INDI/DEAT/_UID'),
				('_death_rin',				  999,	'str',			'INDI/DEAT/RIN'),
				('_death_note_desc',			1,	'str',			'INDI/DEAT/NOTE/_DESCRIPTION'),
				('_crem',						1,	'str',			'INDI/CREM'),		# CREMATIE
				('_crem_uuid',					1,	'str',			'INDI/CREM/_UID'),
				('_crem_rin',					1,	'str',			'INDI/CREM/RIN'),
				('_buri',						1,	'str',			'INDI/BURI'),		# BURIED
				('_buri_uuid',					1,	'str',			'INDI/BURI/_UID'),
				('_buri_rin',				  999,	'str',			'INDI/BURI/RIN'),
				('_buri_note_desc',				1,	'str',			'INDI/BURI/NOTE/_DESCRIPTION'),
				('_desc',						1,	'str',			'INDI/DSCR/_UID'),	# BESCHRIJVING
				('_desc_rin',					1,	'str',			'INDI/DSCR/RIN'),
				('_immi',						1,	'str',			'INDI/IMMI'),		# IMMIGRATIE
				('_immi_uuid',					1,	'str',			'INDI/IMMI/_UID'),
				('_immi_rin',					1,	'str',			'INDI/IMMI/RIN'),
				('_immi_desc',					1,	'str',			'INDI/IMMI/NOTE/_DESCRIPTION'),
				('_emig',						1,	'str',			'INDI/EMIG'),		# EMIGRATIE
				('_emig_uuid',					1,	'str',			'INDI/EMIG/_UID'),
				('_emig_rin',					1,	'str',			'INDI/EMIG/RIN'),
				('_natu',						1,	'str',			'INDI/NATU'),		# NATURALISATIE
				('_natu_uuid',					1,	'str',			'INDI/NATU/_UID'),
				('_natu_rin',					1,	'str',			'INDI/NATU/RIN'),
				('_cens',						1,	'str',			'INDI/CENS'),		# VOLKSTELLING
				('_cens_uuid',				  999,	'str',			'INDI/CENS/_UID'),
				('_cens_rin',				  999,	'str',			'INDI/CENS/RIN'),
				('_reli',						1,	'str',			'INDI/RELI'),		# RELIGIE
				('_reli_uuid',					1,	'str',			'INDI/RELI/_UID'),
				('_reli_rin',					1,	'str',			'INDI/RELI/RIN'),
				('_will',						1,	'str',			'INDI/WILL'),		# TESTAMENT
				('_will_uuid',					1,	'str',			'INDI/WILL/_UID'),
				('_will_rin',					1,	'str',			'INDI/WILL/RIN'),
				('_will_desc',					1,	'str',			'INDI/WILL/NOTE/_DESCRIPTION'),
				('_nati',						1,	'str',			'INDI/NATI'),		# NATIONALITEIT
				('_nati_uuid',					1,	'str',			'INDI/NATI/_UID'),
				('_nati_rin',					1,	'str',			'INDI/NATI/RIN'),
				('gewijzigd',					1,	'str',			'INDI/_UPD'),
				('naam',						1,	'str',			'INDI/NAME'),
				('voornamen',					1,	'str',			'INDI/NAME/GIVN'),
				('roepnaam',					1,	'str',			'INDI/NAME/_AKA'),
				('roepnaam',					1,	'str',			'INDI/NAME/_RNAME'),
				('roepnaam',					1,	'str',			'INDI/NAME/NICK'),
				('achternaam',					1,	'str',			'INDI/NAME/SURN'),
				('meisjesnaam',					1,	'str',			'INDI/NAME/_MARNM'),
				('voorheen',					1,	'str',			'INDI/NAME/_FORMERNAME'),
				('titel',						1,	'str',			'INDI/NAME/NPFX'),
				('toevoeging',					1,	'str',			'INDI/NAME/NSFX'),
				('geslacht',					1,	'str',			'INDI/SEX'),
				('geboortedatum',			  999,	'str',			'INDI/BIRT/DATE'),
				('geboorteplaats',				1,	'str',			'INDI/BIRT/PLAC'),
				('geboorte_opmerking',			1,	'str',			'INDI/BIRT/NOTE'),
				('doop',					  999,	'Doop',			'INDI/BAPM'),
				('doop',					  999,	'Doop',			'INDI/CHR'),
				('nationaliteit',				1,	'str',			'INDI/NATI/PLAC'),
				('beroepen',				  999,	'Beroep',		'INDI/OCCU'),
				('beschrijving',				1,	'str',			'INDI/DSCR'),
				('immigratiedatum',				1,	'str',			'INDI/IMMI/DATE'),
				('immigratieplaats',			1,	'str',			'INDI/IMMI/PLAC'),
				('immigratie_aantekening',		1,	'str',			'INDI/IMMI/NOTE'),
				('emigratiedatum',				1,	'str',			'INDI/EMIG/DATE'),
				('emigratieplaats',				1,	'str',			'INDI/EMIG/PLAC'),
				('datum_naturalisatie',			1,	'str',			'INDI/NATU/DATE'),
				('plaats_naturalisatie',		1,	'str',			'INDI/NATU/PLAC'),
				('datum_volkstelling',		  999,	'str',			'INDI/CENS/DATE'),
				('plaats_volkstelling',		  999,	'str',			'INDI/CENS/PLAC'),
				('ouder_familie_id',			1,	'str',			'INDI/FAMC'),
				('ouder_familie',				1,	'Familie',		''),
				('eigen_familie_id',		  999,	'str',			'INDI/FAMS'),
				('eigen_familie',			  999,	'Familie',		''),
				('bestanden',				  999,	'Bestand',		'INDI/OBJE'),
				('datum_overlijden',			1,	'str',			'INDI/DEAT/DATE'),
				('plaats_overlijden',			1,	'str',			'INDI/DEAT/PLAC'),
				('doodsoorzaak',				1,	'str',			'INDI/DEAT/CAUS'),
				('leeftijd_overlijden',			1,	'str',			'INDI/DEAT/AGE'),
				('aantekening_overlijden',		1,	'str',			'INDI/DEAT/NOTE'),
				('datum_crematie',				1,	'str',			'INDI/CREM/DATE'),
				('plaats_crematie',				1,	'str',			'INDI/CREM/PLAC'),
				('datum_begrafenis',			1,	'str',			'INDI/BURI/DATE'),
				('begraafplaats',				1,	'str',			'INDI/BURI/PLAC'),
				('aantekening_begrafenis',		1,	'str',			'INDI/BURI/NOTE'),
				('bestanden_begrafenis',	  999,	'Bestand',		'INDI/BURI/OBJE'),
				('gebeurtenissen',			  999,	'Gebeurtenis',	'INDI/EVEN'),
				('adressen',				  999,	'Adres',		'INDI/RESI'),
				('opleidingen',				  999,	'Opleiding',	'INDI/EDUC'),
				('bronnen',					  999,	'Bron',			'INDI/SOUR'),
				('huwelijken',				  999,	'Huwelijk',		'INDI/MARR'),
				('huwelijken',				  999,	'Huwelijk',		'INDI/Marriage'),	# FOUTJE MYHERITAGE??
				('aantekening',					1,	'html+',		'INDI/NOTE'),
				('aantekening',					1,	'html+',		'INDI/NOTE/CONC'),
				('aantekening',					1,	'html+',		'INDI/NOTE/CONT'),
				('erfenis',						1,	'str',			'INDI/WILL/NOTE'),
			])
		super().__init__(app, record0, app.metadata[classname])
		self.verwerk(record0)

	def naam_bevat(self, woord):
		'''Geeft True als een van de naamvelden het gevraagde woord bevat (niet hoofdlettergevoelig)'''
		woord = str(woord).upper()
		if self.naam and self.naam.upper().find(woord) >= 0:
			return True
		if self.voornamen and self.voornamen.upper().find(woord) >= 0:
			return True
		if self.achternaam and self.achternaam.upper().find(woord) >= 0:
			return True
		if self.meisjesnaam and self.meisjesnaam.upper().find(woord) >= 0:
			return True
		if self.voorheen and self.voorheen.upper().find(woord) >= 0:
			return True
		# NIETS GEVONDEN, DUS:
		return False

	def vul_verwijzingen(self, dic_ids):
		'''Wordt door de applicatie expliciet aangeroepen om met de ID's de juiste entiteiten te vinden.'''
		if self.ouder_familie_id and self.ouder_familie_id in dic_ids:
			self.ouder_familie = dic_ids[self.ouder_familie_id]
		for id in self.eigen_familie_id:
			self.eigen_familie.append(dic_ids[id])
		for bestand in self.bestanden:
			bestand.vul_verwijzingen(dic_ids)
		for bestand in self.bestanden_begrafenis:
			bestand.vul_verwijzingen(dic_ids)

	def kop(self):
		'''Overschrijft die van de base-class met iets uitgebreiders.'''
		return "%s: %s" % (self._classname, self.naam)

	def naar_tekst_rule(self, rule, tb, tabs, done):
		'''Overschrijft die van de base-class, om child-objecten voor families afwijkend te behandelen.'''

		if rule.prop == 'ouder_familie_id':
			if self.ouder_familie:
				tb.prop(tabs+1, "Ouder-familie", "%s (%s)" % (self.ouder_familie.naam_kort, self.ouder_familie_id))
			else:
				tb.prop(tabs+1, "Ouder-familie", self.ouder_familie_id)
		elif rule.prop == 'ouder_familie':
			pass

		elif rule.prop == 'eigen_familie_id':
			if self.eigen_familie:
				for familie in self.eigen_familie:
					tb.prop(tabs+1, "Eigen familie", "%s (%s)" % (familie.naam_kort, familie.id))
			else:
				for id in self.eigen_familie_id:
					tb.prop(tabs+1, "Eigen familie", id)
		elif rule.prop == 'eigen_familie':
			pass

		else:
			super().naar_tekst_rule(rule, tb, tabs, done)
		done.add(rule.prop)

	def naar_html(self, template=None):
		html  =	template
		html  =	html.replace('###NAAM###', str(self.naam))

		# TABEL EIGENSCHAPPEN:
		hb = HB.HtmlBuilder(fragment=True)
		html = html.replace('###EIGENSCHAPPEN###', super().naar_html(template=None, fragment=True))

		# TABEL FAMILIES:
		hb = HB.HtmlBuilder(fragment=True)
		if self.ouder_familie:
			hb.fragment.literal = HB.Literal(self.ouder_familie.naar_html_persoon())
		else:
			hb.fragment.add_tr_headers("Ouderfamilie", "(onbekend)")
		for familie in self.eigen_familie:
			hb.fragment.literal = HB.Literal(familie.naar_html_persoon(eigen=True))
		html = html.replace('###FAMILIES###', hb.to_html(tab=''))

		return html

	def naar_html_rule(self, rule, hb, done):
		'''Overschrijft die van de base-class om child-objecten voor families over te slaan; deze krijgen een eigen behandeling.'''
		if rule.prop in ['ouder_familie_id', 'ouder_familie', 'eigen_familie_id', 'eigen_familie']:
			pass
		else:
			super().naar_html_rule(rule, hb, done)

