import utils.metadata		as MD
from objects.entiteit 		import Entiteit
from objects.bron 			import Bron
import utils.textbuilder	as TB

class Metadata(Entiteit):
	'''
	De wrapper-class voor de beginrecords die betrekking hebben op de GEDCOM-export zelf.
	'''

	def __init__(self, app, record0):
		classname = self.__class__.__name__
		if not classname in app.metadata:
			app.metadata[classname] = MD.Metadata([
				# PROPERTY      	NUM	TYPE	CONTEXT
				('_regelnummer',	1,	'int',	''),				# VERPLICHT
				('_recordnummer',	1,	'int',	''),				# VERPLICHT
				('_id',				1,	'str',	'HEAD'),			# VERPLICHT (id OF_id)
				('_gedc',			1,	'str',	'HEAD/GEDC'),
				('_timezone',		1,	'str',	'HEAD/DATE/_TIMEZONE'),
				('_rins',			1,	'str',	'HEAD/_RINS'),
				('_uuid',			1,	'str',	'HEAD/_UID'),
				('_project_guid',	1,	'str',	'HEAD/_PROJECT_GUID'),
				('_export_site_id',	1,	'str',	'HEAD/_EXPORTED_FROM_SITE_ID'),
				('_merges',			1,	'str',	'HEAD/_SM_MERGES'),
				('_desc_aware',		1,	'str',	'HEAD/_DESCRIPTION_AWARE'),
				('gedcom_versie',	1,	'str',	'HEAD/GEDC/VERS'),
				('gedcom_formaat',	1,	'str',	'HEAD/GEDC/FORM'),
				('tekstcodering',	1,	'str',	'HEAD/CHAR'),
				('taal',			1,	'str',	'HEAD/LANG'),
				('bron',			1,	'Bron',	'HEAD/SOUR'),
				('indiener_id',		1,	'str',	'HEAD/SUBM'),
				('bestemming',		1,	'str',	'HEAD/DEST'),
				('datum',			1,	'str',	'HEAD/DATE'),
				('tijd',			1,	'str',	'HEAD/DATE/TIME'),
			])
		super().__init__(app, record0, app.metadata[classname])
		self.verwerk(record0)

	def kop(self):
		'''Overschrijft die van de base-class met iets uitgebreiders.'''
		return "%s export %s" % (self._classname, self.datum)

