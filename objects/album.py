from utils.metadata 	import Metadata
from objects.entiteit	import Entiteit

class Album(Entiteit):
	'''
	De wrapper-class voor de records die betrekking hebben op een album.
	Niet duidelijk is hoe dit binnen MyHeritage gebruikt wordt.
	Aan het eind van een export bevinden zich maar een paar regels voor 1 album.
	Met de _PRIN-tag kan wel de oorspronkelijke foto teruggevonden worden, maar het is geen harde bestandsnaam.
	Waarschijnlijk is deze foto gebruikt om verschillende portretfoto's uit te snijden.
	Andere bestanden hebben soms wel een verwijzing naar een album.
	'''

	# (VANWEGE DE GERINGE INHOUD IS DIT BESTAND EEN GOED SJABLOON VOOR NIEUWE ENTITEITEN!)

	def __init__(self, app, record0):
		classname = self.__class__.__name__
		if not classname in app.metadata:
			app.metadata[classname] = Metadata([
				# PROPERTY      	NUM	TYPE	CONTEXT
				('_regelnummer',	1,	'int',	''),				# VERPLICHT
				('_recordnummer',	1,	'int',	''),				# VERPLICHT
				('id',				1,	'str',	'ALBUM'),			# VERPLICHT (id OF_id)
				('_rin',			1,	'str',	'ALBUM/RIN'),
				('_photo',			1,	'str',	'ALBUM/_PHOTO'),
				('_photo_uid',		1,	'str',	'ALBUM/_PHOTO/_UID'),
				('_photo_rin',		1,	'str',	'ALBUM/_PHOTO/_PRIN'),
				('titel',			1,	'str',	'ALBUM/TITL')
			])
		super().__init__(app, record0, app.metadata[classname])
		self.verwerk(record0)

	def kop(self):
		'''Overschrijft die van de base-class met iets uitgebreiders.'''
		if self.titel:
			return "%s %s" % (self._classname, self.titel)
		elif self.id:
			return "%s %s" % (self._classname, self.id)
		else:
			return super().kop