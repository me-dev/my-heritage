class Record:
	'''
	De wrapper-class voor een volledig geinterpreerde regel.
	De ged-tekst heeft als eigenaardigheid dat records met CRLF gescheiden worden.
	Meestal komt 1 regel dan ook over overeen met 1 record.
	Maar er zijn records met meerregelige teksten, waarbij die regels met LF gescheiden worden.
	'''

	def __init__(self, regelnummer, recordnummer, data):
		'''
		Deze constructor crasht zogauw hij iets tegenkomt dat niet begrepen kan worden.
		Regelnummers gaan volgens tekstbestand (CRLF en LF), wat bij meerregelige teksten gaat verschillen van recordnummer.
		Data is alles wat tussen 2 CRLF's staat, wat dus ook LF-gescheiden regels kan bevatten.
		De caller moet met .regelaantal dit aantal regels opvragen (het zal meestal 1 zijn), zodat een volgend record goed begonnen kan worden.
		'''
		self.regelnummer	= regelnummer
		self.recordnummer	= recordnummer
		self.data			= data
		
		# HOEVEEL REGELS (LF) ZITTEN BINNEN DIT RECORD (CRLF)?
		self.regelaantal	= 1 + data.count('\n')
		if self.regelaantal == 1:
			self.regel1		= data
			self.regelrest	= None
		else:
			pos				= data.index('\n')	# HIER WETEN WE DAT IE BESTAAT.
			self.regel1		= data[:pos]
			self.regelrest	= data[pos]
			
		# ONDERDELEN VAN EEN REGEL:
		parts = self.regel1.split(' ')
		count = len(parts)
		if count < 2:
			raise Exception("Te korte regel %i, record %i: %s" % (regelnummer, recordnummer, data))
		try:
			self.level = int(parts[0])
		except:
			raise Exception("Geen getal gevonden aan het begin van regel %i, record %i: %s" % (regelnummer, recordnummer, data))

		# GEWOONLIJK IS HET 2E TOKEN EEN LABEL: "1 NAME Sabine ...", MAAR NIET ALS ER EEN ID IS: "0 @I2@ INDI"
		if count == 3 and parts[1].startswith('@') and parts[1].endswith('@'):
			if parts[2] in ['SUBM', 'INDI', 'FAM', 'SOUR', 'ALBUM']:	# SUBMITTER, INDIVIDUAL, FAMILY, SOURCE, ALBUM:
				self.label = parts[2]
				self.tekst = parts[1]
			else:
				raise Exception("Onbegrijpelijke regel %i, record %i: %s" % (regelnummer, recordnummer, data))
		else:
			self.label = parts[1]
			if count <= 2:
				self.tekst = None
			else:
				# ALLE DATA NA DE 2E SPATIE:
				pos = self.data.index(' ')
				pos = self.data.index(' ', pos+1)
				self.tekst = self.data[pos+1:]
		
	def __repr__(self):
		return "Record('%s')" % self.data

	def __str__(self):
		if self.regelaantal == 1:
			return "Record %i, regel %i, level %i: '%s'='%s'" % (self.recordnummer, self.regelnummer, self.level, self.label, self.tekst)
		else:
			return "Record %i, regel %i, level %i: '%s'='%s ...'" % (self.recordnummer, self.regelnummer, self.level, self.label, self.regel1)
		
