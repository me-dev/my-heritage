from utils.metadata 	import Metadata
from objects.entiteit	import Entiteit

class Beroep(Entiteit):
	'''
	De wrapper-class voor de records die betrekking hebben op beroepen (OCCU).
	'''

	def __init__(self, app, record0):
		classname = self.__class__.__name__
		if not classname in app.metadata:
			app.metadata[classname] = Metadata([
				# PROPERTY      	NUM	TYPE	CONTEXT
				('_regelnummer',	1,	'int',	''),				# VERPLICHT
				('_recordnummer',	1,	'int',	''),				# VERPLICHT
				('naam',			1,	'str',	'OCCU'),			# VERPLICHT
				('_uuid',			1,	'str',	'OCCU/_UID'),
				('_rin',			1,	'str',	'OCCU/RIN'),
				('plaats',			1,	'str',	'OCCU/PLAC'),
				('datum',			1,	'str',	'OCCU/DATE'),
				('aantekening',		1,	'str',	'OCCU/NOTE'),
			])
		super().__init__(app, record0, app.metadata[classname])
		self.verwerk(record0)
