from utils.metadata 	import Metadata
from objects.entiteit	import Entiteit

class Huwelijk(Entiteit):
	'''
	De wrapper-class voor de records die betrekking hebben op huwelijken (MARR).
	'''

	def __init__(self, app, record0):
		classname = self.__class__.__name__
		if not classname in app.metadata:
			app.metadata[classname] = Metadata([
				# PROPERTY      	NUM	TYPE	CONTEXT
				('_regelnummer',	1,	'int',	''),				# VERPLICHT
				('_recordnummer',	1,	'int',	''),				# VERPLICHT
				('_id',				1,	'str',	'MARR'),			# VERPLICHT (id OF_id)
				('_id',				1,	'str',	'Marriage'),		# FOUTJE MYHERITAGE?
				('_uuid',			1,	'str',	'MARR/_UID'),
				('_uuid',			1,	'str',	'Marriage/_UID'),
				('_rin',			1,	'str',	'MARR/RIN'),
				('_rin',			1,	'str',	'Marriage/RIN'),
				('_note_desc',		1,	'str',	'MARR/NOTE/_DESCRIPTION'),
				('datum',			1,	'str',	'MARR/DATE'),
				('datum',			1,	'str',	'Marriage/DATE'),
				('plaats',			1,	'str',	'MARR/PLAC'),
				('plaats',			1,	'str',	'Marriage/PLAC'),
				('aantekening',		1,	'str',	'MARR/NOTE'),
			])
		super().__init__(app, record0, app.metadata[classname])
		self.verwerk(record0)
