from utils.metadata 	import Metadata
from objects.entiteit	import Entiteit

class Doop(Entiteit):
	'''
	De wrapper-class voor de records die betrekking hebben op dopen (BAPM of CHR).
	'''

	def __init__(self, app, record0):
		classname = self.__class__.__name__
		if not classname in app.metadata:
			app.metadata[classname] = Metadata([
				# PROPERTY      	NUM	TYPE		CONTEXT
				('_regelnummer',	1,	'int',		''),				# VERPLICHT
				('_recordnummer',	1,	'int',		''),				# VERPLICHT
				('_id',				1,	'str',		'BAPM'),			# VERPLICHT (id OF_id)
				('_id',				1,	'str',		'CHR'),				# VERPLICHT (id OF_id)
				('_uuid',			1,	'str',		'BAPM/_UID'),
				('_uuid',			1,	'str',		'CHR/_UID'),
				('_rin',			1,	'str',		'BAPM/RIN'),
				('_rin',			1,	'str',		'CHR/RIN'),
				('_note_desc',		1,	'str',		'BAPM/NOTE/_DESCRIPTION'),
				('_note_desc',		1,	'str',		'CHR/NOTE/_DESCRIPTION'),
				('datum',			1,	'str',		'BAPM/DATE'),
				('datum',			1,	'str',		'CHR/DATE'),
				('plaats',			1,	'str',		'BAPM/PLAC'),
				('plaats',			1,	'str',		'CHR/PLAC'),
				('leeftijd',		1,	'str',		'BAPM/AGE'),
				('leeftijd',		1,	'str',		'CHR/AGE'),
				('bestand',			1,	'Bestand',	'BAPM/OBJE'),
				('bestand',			1,	'Bestand',	'CHR/OBJE'),
				('aantekening',		1,	'str',		'BAPM/NOTE'),
				('aantekening',		1,	'str',		'CHR/NOTE'),
			])
		super().__init__(app, record0, app.metadata[classname])
		self.verwerk(record0)

	def is_gelijk_aan(self, other):
		# ER BESTAAN PERSONEN MET EEN BAPM- EN EEN CHR-TAG MET DEZELFDE INHOUD!

		if self.datum and not other.datum:
			return False
		if other.datum and not self.datum:
			return False
		if self.datum != other.datum:
			return False

		if self.plaats and not other.plaats:
			return False
		if other.plaats and not self.plaats:
			return False
		if self.plaats != other.plaats:
			return False

		if self.aantekening and not other.aantekening:
			return False
		if other.aantekening and not self.aantekening:
			return False
		if self.aantekening != other.aantekening:
			return False

		# ALLE CHECKS GEPASSEERD, DUS:
		return True