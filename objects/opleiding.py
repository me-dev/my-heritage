from utils.metadata 	import Metadata
from objects.entiteit	import Entiteit

class Opleiding(Entiteit):
	'''
	De wrapper-class voor de records die betrekking hebben op opleidingen (EDUC).
	Wordt weinig, en ook slecht gebruikt, maar kan vaker voorkomen voor 1 persoon.
	'''

	def __init__(self, app, record0):
		classname = self.__class__.__name__
		if not classname in app.metadata:
			app.metadata[classname] = Metadata([
				# PROPERTY      	NUM	TYPE	CONTEXT
				('_regelnummer',	1,	'int',	''),				# VERPLICHT
				('_recordnummer',	1,	'int',	''),				# VERPLICHT
				('naam',			1,	'str',	'EDUC'),			# VERPLICHT (id OF_id)
				('_uuid',			1,	'str',	'EDUC/_UID'),
				('_rin',			1,	'str',	'EDUC/RIN'),
				('plaats',			1,	'str',	'EDUC/PLAC'),
				('datum',			1,	'str',	'EDUC/DATE'),
				('aantekening',		1,	'str',	'EDUC/NOTE'),
			])
		super().__init__(app, record0, app.metadata[classname])
		self.verwerk(record0)
