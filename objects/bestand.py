import utils.filesystem_functions	as FS
from utils.metadata 				import Metadata
from objects.entiteit 				import Entiteit

class Bestand(Entiteit):
	'''
	De wrapper-class voor de records die betrekking hebben op een bestand (foto/PDF).
	'''

	def __init__(self, app, record0):
		classname = self.__class__.__name__
		if not classname in app.metadata:
			app.metadata[classname] = Metadata([
				# PROPERTY      	NUM	TYPE	CONTEXT
				('_regelnummer',	1,	'int',		''),				# VERPLICHT
				('_recordnummer',	1,	'int',		''),				# VERPLICHT
				('_id',				1,	'str',		'OBJE'),			# VERPLICHT (id OF_id)
				('_prim',			1,	'str',		'OBJE/_PRIM'),
				('_prim_cutout',	1,	'str',		'OBJE/_PRIM_CUTOUT'),
				('_position',		1,	'str',		'OBJE/_POSITION'),
				('_photo_rin',		1,	'str',		'OBJE/_PHOTO_RIN'),
				('_cutout',			1,	'str',		'OBJE/_CUTOUT'),
				('_parentrin',		1,	'str',		'OBJE/_PARENTRIN'),
				('_scan',			1,	'str', 		'OBJE/_SCAN'),
				('formaat',			1,	'str',		'OBJE/FORM'),
				('titel',			1,	'str',		'OBJE/TITL'),
				('_file',			1,	'str',		'OBJE/FILE'),		# VOL PAD ZOALS IN DE GEDCOM-FILE.
				('naam',			1,	'str',		''),				# KRIJGT ACHTERAF DE BESTANDSNAAM UIT _file.
				('link',			1,	'url',		''),				# KRIJGT ACHTERAF DE VOLLE URL NAAR HET WERKELIJKE BESTAND.
				('grootte',			1,	'int',		'OBJE/_FILESIZE'),
				('datum',			1,	'str',		'OBJE/_DATE'),
				('plaats',			1,	'str',		'OBJE/_PLACE'),
				('album_id',		1,	'str',		'OBJE/_ALBUM'),
				('album',			1,	'Album',	''),
			])
		super().__init__(app, record0, app.metadata[classname])
		self.verwerk(record0)

	def verwerk(self, record):
		super().verwerk(record)
		self.naam = FS.name_sec(self._file)		# E:\Vriesky_Photos\P103_894_1030.jpg -> P103_894_1030.jpg
		if self.pad_bestaat():
			self.link = "file://%s" % self.pad(True)

	def vul_verwijzingen(self, dic_ids):
		'''Wordt door de applicatie expliciet aangeroepen om met de ID's de juiste entiteiten te vinden.'''
		if self.album_id and self.album_id in dic_ids:
			self.album = dic_ids[self.album_id]

	def pad(self, absolute=False):
		absolute = bool(absolute)
		path	 = "%s/%s" % (self._app.plaatjesdir, self.naam)
		if absolute:
			return FS.path_absolute(path)
		else:
			return path

	def pad_bestaat(self):
		return FS.exists_file(self.pad())

	def is_afbeelding(self):
		'''
		Of het bestand een plaatje is, bijv. om in de HTML te tonen.
		Op dit moment (2021/04) komen er maar 3 formaten voor: jpg, jpeg en pdf.
		'''
		return FS.extension_image(self.formaat)

	def naar_pdf_rules(self, pw):
		'''
		In addition to normal entities, we add the image to the PDF, but not the duplicates.
		'''
		super().naar_pdf_rules(pw)

		if self.link and not self._parentrin:
			path = self.link.replace("file://", "")
			if FS.extension_image(path):
				pw.image(path, width=100, height=100, absolute=False)
