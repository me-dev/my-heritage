# my-heritage

## Inleiding:

Python-project om exports uit MyHeritage makkelijk mee te kunnen ontsluiten:
- Leest de gedcom-file in.
- Vult entiteit-objecten
- Print, doorzoekt en filtert het resultaat.
- Genereert eenvoudige HTML-pagina's voor personen, families enz.

Dit project is eigenlijk bedoeld om familieleden gemakkelijk toegang te geven tot de scripts.<br>
Het project zelf bevat echter geen vertrouwelijke informatie.<br>
Iedere bezoeker mag eruit kopiëren wat hij wil; alles onder eigen verantwoording.

## Vereisten:

- Een Linux-computer met om het even welke recente distributie (bijv. Ubuntu of Mint).
- Een directory-structuur met bij voorkeur onderstaande indeling:<br>
	(Namen zijn evt. congifureerbaar.)
```
    Je-start-directory/
        Data/
            Tekst.ged       Het GEDCOM-bestand gedownload van MyHeritage.
                            Omdat hierin 2 soorten regeleindes gebruikt worden, moet je dit niet wijzigen!
                            (Tekst-editors willen dit nog wel eens rechttrekken.)
            Fotos/          Alle foto's (en PDF's) gedownload van MyHeritage.
        my-heritage/        De inhoud van dit project.
```

## Gebruik:

Gebruiksaanwijzing:
```
    $ ./start.py
```
Check de configuratie (.cfg-file):
```
    $ ./start.py check
```
Check het gedcom-bestand op corrupte tekens:
```
    $ ./start.py repareer
```
Check of alle regels begrepen kunnen worden:
(gedcom is een nogal open standaard, en er kan wat nieuws toegevoegd zijn)
```
    $ ./start.py dump -1
```

